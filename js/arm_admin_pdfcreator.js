var arm_pdfcreator_localize_data=arm_pdfcreator_localize;
var arm_pdfcreator_attachment_emails_templates=arm_pdfcreator_localize_data.arm_pdfcreator_attachment_email_templates;
var arm_pdfcreator_attachment_templates_for_armcourse=arm_pdfcreator_localize_data.arm_pdfcreator_attachment_templates_for_armcourse;
jQuery(document).on('click', '.arm_add_new_message_btn', function () {
	var arm_message_type=jQuery('#arm_message_type').val();
	var $add_message_wrapper_form = jQuery('#arm_add_message_wrapper_frm');
	if (jQuery.inArray(arm_message_type, arm_pdfcreator_attachment_emails_templates) != -1)
	{
		if($add_message_wrapper_form.find('#arm_automated_email_cardpdf_attachment').length>0 || $add_message_wrapper_form.find('#arm_automated_email_invoicepdf_attachment').length>0){

			$add_message_wrapper_form.find('.arm_pdfcreator_automated_email_switch').show();
		  	$add_message_wrapper_form.find('#arm_automated_email_cardpdf_attachment').prop('checked', false);
		  	$add_message_wrapper_form.find('#arm_automated_email_invoicepdf_attachment').prop('checked', false);
		}  
	}else{
		if($add_message_wrapper_form.find('#arm_automated_email_cardpdf_attachment').length>0 || $add_message_wrapper_form.find('#arm_automated_email_invoicepdf_attachment').length>0){

			$add_message_wrapper_form.find('.arm_pdfcreator_automated_email_switch').hide();
			$add_message_wrapper_form.find('#arm_automated_email_cardpdf_attachment').prop('checked', false);
			$add_message_wrapper_form.find('#arm_automated_email_invoicepdf_attachment').prop('checked', false);
		}
	}
	if (jQuery.inArray(arm_message_type, arm_pdfcreator_attachment_templates_for_armcourse) != -1)
	{
		if($add_message_wrapper_form.find('#arm_email_certificatepdf_attachment').length>0){

			$add_message_wrapper_form.find('.arm_pdfcreator_automated_email_switch_course_certificate').show();
		  	$add_message_wrapper_form.find('#arm_email_certificatepdf_attachment').prop('checked', false);
		}  
	}else{
		if($add_message_wrapper_form.find('#arm_email_certificatepdf_attachment').length>0){

			$add_message_wrapper_form.find('.arm_pdfcreator_automated_email_switch_course_certificate').hide();
			$add_message_wrapper_form.find('#arm_email_certificatepdf_attachment').prop('checked', false);
		}
	}
});	
jQuery(document).on('change','.arm_message_period_post #arm_message_type', function () {
	var arm_message_type=jQuery('#arm_message_type').val();
	var $add_message_wrapper_form = jQuery('#arm_add_message_wrapper_frm');
	if (jQuery.inArray(arm_message_type, arm_pdfcreator_attachment_emails_templates) != -1)
	{
		if($add_message_wrapper_form.find('#arm_automated_email_cardpdf_attachment').length>0 || $add_message_wrapper_form.find('#arm_automated_email_invoicepdf_attachment').length>0){

	  		$add_message_wrapper_form.find('.arm_pdfcreator_automated_email_switch').show();
	  	}	
	}else{
		if($add_message_wrapper_form.find('#arm_automated_email_cardpdf_attachment').length>0 || $add_message_wrapper_form.find('#arm_automated_email_invoicepdf_attachment').length>0){

			$add_message_wrapper_form.find('.arm_pdfcreator_automated_email_switch').hide();
			$add_message_wrapper_form.find('#arm_automated_email_cardpdf_attachment').prop('checked', false);
			$add_message_wrapper_form.find('#arm_automated_email_invoicepdf_attachment').prop('checked', false);
		}	
	}
	if (jQuery.inArray(arm_message_type, arm_pdfcreator_attachment_templates_for_armcourse) != -1)
	{
		if($add_message_wrapper_form.find('#arm_email_certificatepdf_attachment').length>0){

			$add_message_wrapper_form.find('.arm_pdfcreator_automated_email_switch_course_certificate').show();
	  	}	
	}else{
		if($add_message_wrapper_form.find('#arm_email_certificatepdf_attachment').length>0){

			$add_message_wrapper_form.find('.arm_pdfcreator_automated_email_switch_course_certificate').hide();
			$add_message_wrapper_form.find('#arm_email_certificatepdf_attachment').prop('checked', false);
		}	
	}
});	
jQuery(document).on('click', '#arm_email_cardpdf_attachment', function () {
	if (jQuery(this).is(":checked")) {
		jQuery('.arm_email_pdf_card_dropdown').show();
	}else{
		jQuery('.arm_email_pdf_card_dropdown').hide();
	}	
});
jQuery(document).on('click', '#arm_automated_email_cardpdf_attachment', function () {
	if (jQuery(this).is(":checked")) {
		jQuery('.arm_automated_email_pdf_card_dropdown').show();
	}else{
		jQuery('.arm_automated_email_pdf_card_dropdown').hide();
	}	
});
