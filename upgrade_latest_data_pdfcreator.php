<?php 
global $armpdfcreator_newdbversion;
$armpdfcreator_newdbversion = '1.1';
update_option('arm_pdfcreator_version', $armpdfcreator_newdbversion);

$arm_version_updated_date_key = 'arm_pdf_creator_version_updated_date_'.$armpdfcreator_newdbversion;
$arm_version_updated_date = current_time('mysql');
update_option($arm_version_updated_date_key, $arm_version_updated_date);