<?php 
/*
  Plugin Name: ARMember - PDF Creator Addon
  Description: Extension for ARMember plugin to Generate PDF for Payment Invoice & Membership Card
  Version: 1.3
  Plugin URI: https://www.armemberplugin.com
  Author: Repute Infosystems
  AUthor URI: https://www.armemberplugins.com
  Text Domain: ARMPdfCreator
 */

define('ARM_PDF_CREATOR_DIR_NAME', 'armemberpdfcreator');
define('ARM_PDF_CREATOR_DIR', WP_PLUGIN_DIR . '/' . ARM_PDF_CREATOR_DIR_NAME);

require_once ARM_PDF_CREATOR_DIR.'/autoload.php';