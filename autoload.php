<?php 
if (is_ssl()) {
    define('ARM_PDF_CREATOR_URL', str_replace('http://', 'https://', WP_PLUGIN_URL . '/' . ARM_PDF_CREATOR_DIR_NAME));
} else {
    define('ARM_PDF_CREATOR_URL', WP_PLUGIN_URL . '/' . ARM_PDF_CREATOR_DIR_NAME);
}

define('ARM_PDF_CREATOR_CORE_DIR', ARM_PDF_CREATOR_DIR . '/core');
define('ARM_PDF_CREATOR_LIB_DIR', ARM_PDF_CREATOR_DIR . '/lib');
define('ARM_PDF_CREATOR_CLASSES_DIR', ARM_PDF_CREATOR_CORE_DIR . '/classes');
define('ARM_PDF_CREATOR_VIEWS_DIR', ARM_PDF_CREATOR_CORE_DIR . '/views');
define('ARM_PDF_CREATOR_JS_URL', ARM_PDF_CREATOR_URL . '/js');
define('ARM_PDF_CREATOR_CSS_URL', ARM_PDF_CREATOR_URL . '/css');
define('ARM_PDF_CREATOR_TEXTDOMAIN', 'ARMPdfCreator');
if(!defined('ARMADDON_STORE_URL')){
    define( 'ARMADDON_STORE_URL', 'https://www.armemberplugin.com/' );
}    

global $arm_pdfcreator_version, $arm_pdfcreator_class, $arm_pdfcreator_attachment_templates, $arm_pdf_upload_dir_name, $arm_pdfcreator_is_compatible, $arm_pdfcreator_attachment_templates_for_armcourse;
$arm_pdfcreator_version = '1.3';
$arm_pdf_upload_dir_name='arm_pdfs';
$arm_pdfcreator_attachment_templates=array('new-reg-user-with-payment','on_new_subscription','on_change_subscription','on_change_subscription_by_admin','on_renew_subscription','on_recurring_subscription','on_new_subscription_post','on_renew_subscription_post','on_recurring_subscription_post');
$arm_pdfcreator_attachment_templates_for_armcourse=array('arm_courses_notify_when_complete_course');
global $wp_version;

global $armpdfcreator_newdbversion;
$wp_upload_dir = wp_upload_dir();

$armemberpdfcreator_upload_dir = $wp_upload_dir['basedir'] . '/armember/'.$arm_pdf_upload_dir_name;
$armemberpdfcreator_font_dir = $armemberpdfcreator_upload_dir.'/fonts';

define( 'ARMPDFCREATOR_FONT_DIR',$armemberpdfcreator_font_dir);

if (!class_exists('ARM_PDFCreator')) {

    class ARM_PDFCreator {

        function __construct() {
            register_activation_hook(ARM_PDF_CREATOR_DIR.'/armemberpdfcreator.php', array('ARM_PDFCreator', 'arm_pdfcreator_install'));
            register_uninstall_hook(ARM_PDF_CREATOR_DIR.'/armemberpdfcreator.php', array( 'ARM_PDFCreator', 'arm_pdfcreator_uninstall'));
            add_action('init', array(&$this, 'arm_pdfcreator_db_check'), 100);
            add_action('plugins_loaded', array(&$this, 'arm_pdfcreator_load_textdomain'));
            add_action('admin_notices', array(&$this, 'arm_pdf_creator_admin_notices'), 100);
			add_action('admin_init', array(&$this, 'upgrade_data_pdfcreator'));
            //if(!empty($this->armpdfcreator_license_status())){
                add_action('init', array(&$this, 'arm_load_mpdf_library'));
                add_action('admin_enqueue_scripts', array(&$this, 'arm_pdfcreator_set_js'), 11);
                add_action('wp_enqueue_scripts', array(&$this, 'arm_pdfcreator_for_setting_up_scripts'));
                add_action('armpdfcreator_import_basic_fonts', array( $this, 'armpdfcreator_import_basic_fonts_files' ) ); 
            //}    
            add_action( 'admin_enqueue_scripts', array( &$this, 'arm_pdfcreator_license_scripts' ), 20 );
        }
        
        public static function arm_pdfcreator_install() {
            global $arm_pdfcreator_class, $arm_pdf_upload_dir_name, $arm_pdfcreator_version;
            $arm_pdfcreator_get_plugin_version = get_option('arm_pdfcreator_version');
            if (!isset($arm_pdfcreator_get_plugin_version) || $arm_pdfcreator_get_plugin_version == '') {
                update_option('arm_pdfcreator_version', $arm_pdfcreator_version);
            }
            if($arm_pdfcreator_class->arm_pdfcreator_is_compatible()){
                global $arm_pdfcreator_version,$ARMember,$wpdb;
                $arm_email_template_table = $ARMember->tbl_arm_email_templates;
                $arm_email_template_pdf_attachment = $wpdb->get_results("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' AND TABLE_NAME = '".$arm_email_template_table."' AND column_name = 'arm_email_pdf_attachment_status'");//phpcs:ignore
                if(empty($arm_email_template_pdf_attachment)){
                    $wpdb->query("ALTER TABLE `{$arm_email_template_table}` ADD `arm_email_pdf_attachment_status` TEXT NOT NULL AFTER `arm_template_status`");//phpcs:ignore
                }

                $arm_auto_message_table = $ARMember->tbl_arm_auto_message;
                $arm_auto_email_pdf_attachment = $wpdb->get_results("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".DB_NAME."' AND TABLE_NAME = '".$arm_auto_message_table."' AND column_name = 'arm_email_pdf_attachment_status'");//phpcs:ignore
                if(empty($arm_auto_email_pdf_attachment)){
                    $wpdb->query("ALTER TABLE `{$arm_auto_message_table}` ADD `arm_email_pdf_attachment_status` TEXT NOT NULL AFTER `arm_message_admin_message`");//phpcs:ignore
                }

                $wp_upload_dir  = wp_upload_dir();
                $upload_dir = $wp_upload_dir['basedir'] . '/armember/'.$arm_pdf_upload_dir_name;

                wp_mkdir_p($upload_dir);

                $armemberpdfcreator_font_dir = $upload_dir.'/fonts';

                wp_mkdir_p( $armemberpdfcreator_font_dir );

                do_action( 'armpdfcreator_import_basic_fonts' );
            }    
        }

        public static function arm_pdfcreator_uninstall() {
            global $arm_pdfcreator_version,$ARMember,$wpdb;
            $arm_email_template_table = $ARMember->tbl_arm_email_templates;
            $arm_auto_message_table = $ARMember->tbl_arm_auto_message;
            delete_option('arm_pdfcreator_version');
            delete_option('arm_pdfcreator_google_api_key');
            delete_option('arm_pdfcreator_google_fonts_data');
            delete_option('arm_pdfcreator_extra_fonts');
             
            //$wpdb->query("ALTER TABLE `{$arm_email_template_table}` DROP `arm_email_pdf_attachment_status`");
            //$wpdb->query("ALTER TABLE `{$arm_auto_message_table}` DROP `arm_email_pdf_attachment_status`");
        }
        function armpdfcreator_import_basic_fonts_files(){

            $source_dir = ARM_PDF_CREATOR_DIR . '/lib/mpdf/vendor/mpdf/mpdf/ttfonts';
            $destination_dir = ARMPDFCREATOR_FONT_DIR;

            if( !is_dir( $destination_dir ) ){
                wp_mkdir_p( $destination_dir );
            }

            if( is_dir( $source_dir ) ){
                if( $dh = opendir( $source_dir ) ){
                    while( ($file = readdir($dh)) !== false ){
                        if( '.' != $file && '..' != $file ){
                            $source = $source_dir.'/'.$file;
                            $target = $destination_dir . '/' .$file;
                            try{
                                copy( $source, $target );
                            } catch(Exception $e){
                                //destination directory does not have permission
                            }
                        }
                    }
                }
            }
        }
        public static function arm_pdfcreator_db_check() {
            global $arm_pdfcreator_class;
            $arm_pdfcreator_get_plugin_version = get_option('arm_pdfcreator_version');

            if (!isset($arm_pdfcreator_get_plugin_version) || $arm_pdfcreator_get_plugin_version == '') {
                $arm_pdfcreator_class->arm_pdfcreator_install();
            }
        }

        public static function arm_pdfcreator_load_textdomain() {
            load_plugin_textdomain('ARMPdfCreator', false, dirname( plugin_basename(__FILE__)). '/languages/' );
        }
		
        static function arm_pdfcreator_get_armember_version() {
            $arm_pdfcreator_armemberplugin_version = get_option('arm_version');
            return isset($arm_pdfcreator_armemberplugin_version) ? $arm_pdfcreator_armemberplugin_version : 0;
        }

        static function arm_pdfcreator_is_armember_plugin_installed() {
            include_once(ABSPATH . 'wp-admin/includes/plugin.php');
            return is_plugin_active('armember/armember.php');
        }

        public static function arm_pdfcreator_is_plugin_version_comatible() {
            global $arm_pdfcreator_class;   
            if ($arm_pdfcreator_class->arm_pdfcreator_is_armember_plugin_installed() && version_compare($arm_pdfcreator_class->arm_pdfcreator_get_armember_version(), '4.6', '>=')) {
                return true;
            } else {
                return false;
            }
        }

        public static function arm_pdfcreator_is_compatible() {
            global $arm_pdfcreator_class;
            $arm_get_php_version = (function_exists('phpversion')) ? phpversion() : 0; 
            if( $arm_pdfcreator_class->arm_pdfcreator_is_plugin_version_comatible() && version_compare($arm_get_php_version, '7.1', '>=') ) :
                return true;
            else :
                return false;
            endif;
        }
        function armpdfcreator_license_status(){
            $armpdfcreator_license_status=get_option('armpdfcreator_license_status');
            return ($armpdfcreator_license_status=='valid')?true:false;
        }
        public static function arm_pdf_creator_admin_notices() {
            global $pagenow, $arm_slugs, $arm_pdfcreator_class;    
            if($pagenow == 'plugins.php' || (isset($_REQUEST['page']) && in_array($_REQUEST['page'], (array) $arm_slugs))) {
                $arm_get_php_version = (function_exists('phpversion')) ? phpversion() : 0;
                $armpdfcreator_license_status=get_option('armpdfcreator_license_status');
                $armpdfcreator_license_key=get_option('armpdfcreator_license_key');
                $armpdfcreator_page_slug=(isset($_REQUEST['page']))?sanitize_text_field($_REQUEST['page']):'';
                if(($armpdfcreator_license_status!='valid' || empty($armpdfcreator_license_key)) && $armpdfcreator_page_slug!='arm_manage_license' && $arm_pdfcreator_class->arm_pdfcreator_is_armember_plugin_installed()){
                    $admin_doc_license_url = "https://www.armemberplugin.com/documents/activate-armember-addon-license/";
                    printf( "<div class='notice notice-error arf-notice-update-warning is-dismissible' style='border-left-color:#dc3232;display:block;'><p><b>" . esc_html__("Please activate the ARMember - PDF Creator Addon License from ARMember ➝ Licensing page to get future updates. For more information, %1\$s click here %2\$s.", 'ARMPdfCreator') . "</b></p></div>","<a href='".esc_url( $admin_doc_license_url )."' target='_blank'>",'</a>');
                }else if (!$arm_pdfcreator_class->arm_pdfcreator_is_armember_plugin_installed()) {
                    echo "<div class='updated updated_notices'><p>" . esc_html__('ARMember - PDF Creator plugin requires ARMember Plugin installed and active', 'ARMPdfCreator') . "</p></div>";
                } else if (!$arm_pdfcreator_class->arm_pdfcreator_is_plugin_version_comatible()) {
                    echo "<div class='updated updated_notices'><p>" . esc_html__('ARMember - PDF Creator plugin requires ARMember plugin installed with version 4.5 or higher.', 'ARMPdfCreator') . "</p></div>";
                }
                else if(!$arm_pdfcreator_class->arm_pdfcreator_is_compatible()) {
                    echo '<div class="notice notice-warning" style="display:block;">';
                    echo '<p>'.esc_html__('mPDF Library for ARMember PDF creator required Minimum PHP version 7.1 or greater.', 'ARMPdfCreator').'</p>';
                    echo '</div>';
                }
            }
        }

        function arm_pdfcreator_license_scripts(){
            global $arm_pdfcreator_version, $arm_version;
            wp_register_script('arm-pdfcreator-license', ARM_PDF_CREATOR_URL . '/js/arm_pdfcreator_license.js', array(), $arm_pdfcreator_version);
            if( isset($_REQUEST['page']) && $_REQUEST['page'] == 'arm_manage_license')
            {
                wp_enqueue_script( 'arm-pdfcreator-license');
                global $wp_styles;
                $srcs = array_map('basename', (array) wp_list_pluck($wp_styles->registered, 'src') );
                if (!in_array('arm_pdfcreator_admin.css', $srcs)) {
                    wp_register_style( 'arm_pdfcreator_admin_css', ARM_PDF_CREATOR_URL . '/css/arm_pdfcreator_admin.css', array(), $arm_pdfcreator_version );
                }    
                wp_enqueue_style( 'arm_pdfcreator_admin_css' );
            }
        }
		function arm_pdfcreator_set_js(){
            global $arm_pdfcreator_version,$arm_pdfcreator_attachment_templates, $arm_pdfcreator_attachment_templates_for_armcourse;
            wp_register_script('arm_admin_pdfcreator_js', ARM_PDF_CREATOR_JS_URL.'/arm_admin_pdfcreator.js', array(), $arm_pdfcreator_version);
            $arm_pdfcreator_localize_array=array('arm_pdfcreator_attachment_email_templates'=>$arm_pdfcreator_attachment_templates, 'arm_pdfcreator_attachment_templates_for_armcourse'=>$arm_pdfcreator_attachment_templates_for_armcourse);
            wp_localize_script( 'arm_admin_pdfcreator_js', 'arm_pdfcreator_localize', $arm_pdfcreator_localize_array);
            if (isset($_REQUEST['page']) && ($_REQUEST['page']=="arm_email_notifications" || $_REQUEST['page']=="arm_general_settings")) {
                wp_enqueue_script('arm_admin_pdfcreator_js');
            }    
             wp_register_style('arm_pdfcreator_admin_css', ARM_PDF_CREATOR_CSS_URL . '/arm_pdfcreator_admin.css', array(), $arm_pdfcreator_version);
            if (isset($_REQUEST['page']) && ($_REQUEST['page']=="arm_transactions" || $_REQUEST['page']=="arm_email_notifications" || $_REQUEST['page']=="arm_manage_members" || $_REQUEST['page']=="arm_general_settings")) {
                wp_enqueue_style( 'arm_pdfcreator_admin_css');
            }
        }
        function arm_pdfcreator_for_setting_up_scripts(){
            global $arm_pdfcreator_version,$ARMember;
            wp_register_style('arm_pdfcreator_front_css', ARM_PDF_CREATOR_CSS_URL . '/arm_pdfcreator_front.css', array(), $arm_pdfcreator_version);

            $is_arm_pdf_front_page = $ARMember->is_arm_front_page();
            if(!empty($is_arm_pdf_front_page)){
                wp_enqueue_style( 'arm_pdfcreator_front_css');
            }    
        }
		function upgrade_data_pdfcreator() {
			global $arm_pdfcreator_version,$armpdfcreator_newdbversion,$arm_pdfcreator_class;
	
			if (!isset($arm_pdfcreator_version) || $arm_pdfcreator_version == ""){
				$armpdfcreator_newdbversion = get_option('arm_pdfcreator_version');
            }
            if (version_compare($arm_pdfcreator_version, '1.3', '<')) {
                $path = ARM_PDF_CREATOR_VIEWS_DIR . '/upgrade_latest_data_arm_pdfcreator.php';
                include($path);
            }
            
		}

        function arm_pdfcreator_messages() {
            $alertMessages = array(
                'delOptInsConfirm' => __("Are you sure to delete configuration?", 'ARMPdfCreator'),
            );
            return $alertMessages;
        }
		
		
		function armpdfcreator_get_remote_post_params($plugin_info = "") {
			global $wpdb;
	
			$action = "";
			$action = $plugin_info;
	
			if (!function_exists('get_plugins')) {
				require_once(ABSPATH . 'wp-admin/includes/plugin.php');
			}
			$plugin_list = get_plugins();
			$site_url = home_url();
			$plugins = array();
	
			$active_plugins = get_option('active_plugins');
	
			foreach ($plugin_list as $key => $plugin) {
				$is_active = in_array($key, $active_plugins);
	
				//filter for only armember ones, may get some others if using our naming convention
				if (strpos(strtolower($plugin["Title"]), "armemberpdfcreator") !== false) {
					$name = substr($key, 0, strpos($key, "/"));
					$plugins[] = array("name" => $name, "version" => $plugin["Version"], "is_active" => $is_active);
				}
			}
			$plugins = json_encode($plugins);
	
			//get theme info
			$theme = wp_get_theme();
			$theme_name = $theme->get("Name");
			$theme_uri = $theme->get("ThemeURI");
			$theme_version = $theme->get("Version");
			$theme_author = $theme->get("Author");
			$theme_author_uri = $theme->get("AuthorURI");
	
			$im = is_multisite();
			$sortorder = get_option("armSortOrder");
	
			$post = array("wp" => get_bloginfo("version"), "php" => phpversion(), "mysql" => $wpdb->db_version(), "plugins" => $plugins, "tn" => $theme_name, "tu" => $theme_uri, "tv" => $theme_version, "ta" => $theme_author, "tau" => $theme_author_uri, "im" => $im, "sortorder" => $sortorder);
	
			return $post;
		}

        function arm_load_mpdf_library() {
            global $arm_pdfcreator_class, $wpdb,$ARMember;
            if(!$this->arm_pdfcreator_is_compatible())
            {
                return;
            }
            
            if(isset($_REQUEST['member_id']) && !empty($_REQUEST['member_id']) && is_user_logged_in()) {
                $user_id = get_current_user_id();
                if($_REQUEST['member_id']!=$user_id && !user_can($user_id, 'administrator') && !user_can($user_id, 'arm_view_pdf_cap')){
                    return;
                }
            }
            if(isset($_REQUEST['invoice_id']) && !empty($_REQUEST['invoice_id']) && is_user_logged_in()) {
                $user_id = get_current_user_id();
                $log_data = $wpdb->get_row("SELECT * FROM `" . $ARMember->tbl_arm_payment_log . "` WHERE `arm_log_id`='" . $_REQUEST['invoice_id'] . "' and arm_user_id='".$user_id."'", ARRAY_A);//phpcs:ignore
                if(empty($log_data) && !user_can($user_id, 'administrator') && !user_can($user_id, 'arm_view_pdf_cap')) {
                    return;
                }
            }
            global $arm_pdfcreator_mpdf;
            global $arm_pdf_constructor;

            $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];
            
            $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            $fontData=[
                    'arial' => [
                        'R' => 'arial.ttf'
                    ],
                    'courier_new' => [
                        'R' => 'cour_new.ttf'
                    ],
                    'courier' => [
                        'R' => 'Courier.ttf'
                    ],
                    'geneva' => [
                        'R' => 'Geneva.ttf'
                    ],
                    'helvetica' => [
                        'R' => 'Helvetica.ttf'
                    ],
                    'lucida_grande' => [
                        'R' => 'LucidaGrande.ttf',
                        'B' => 'LucidaGrandeBold.ttf'
                    ],
                    'lucida_sans_unicode' => [
                        'R' => 'lucida-sans-unicode.ttf'
                    ],
                    'monospace' => [
                        'R' => 'Monospace.ttf',
                    ],
                    'dejavuserif' => [
                        'R' => 'DejaVuSerif.ttf',
                        'B' => 'DejaVuSerif-Bold.ttf',
                        'I' => 'DejaVuSerif-Italic.ttf',
                        'BI'=> 'DejaVuSerif-BoldItalic.ttf',                     
                    ],
                    'tahoma' => [
                        'R' => 'Tahoma.ttf'
                    ],
                    'timesnewroman' => [
                        'R' => 'Timesr.ttf'
                    ],
                    'verdana' => [
                        'R' => 'Verdana.ttf'
                    ]
                ];
                $arm_pdfcreator_get_fonts_arr=$this->arm_pdfcreator_get_fonts_arr();
                $arm_pdfcreator_get_google_fonts_arr=$this->google_api_fonts();
               
                $arm_pdfcreator_extra_fonts = get_option('arm_pdfcreator_extra_fonts', '');
                if(!empty($arm_pdfcreator_extra_fonts)){
                    if(count($arm_pdfcreator_extra_fonts)>0){
                        foreach ($arm_pdfcreator_extra_fonts as $sf_key) {
                            if(isset($arm_pdfcreator_get_fonts_arr[$sf_key])){
                                foreach ($arm_pdfcreator_get_fonts_arr[$sf_key] as $arm_pdfcreator_fonts_data) {
                                    if(count($arm_pdfcreator_fonts_data)){
                                        $arm_pdf_ttf_files_arr=array();
                                        foreach ($arm_pdfcreator_fonts_data as $skey => $sfile_url) {
                                           if (filter_var($sfile_url, FILTER_VALIDATE_URL)) {
                                                $fname_font = basename($sfile_url);
                                                $arm_pdf_ttf_files_arr[$skey]=$fname_font;
                                           }else{
                                                $arm_pdf_ttf_files_arr[$skey]=$sfile_url;
                                           } 
                                        }
                                        $fontData[$sf_key]=$arm_pdf_ttf_files_arr;
                                    } 
                                }                                   
                            }
                            if(count($arm_pdfcreator_get_google_fonts_arr)>0){
                                if(isset($arm_pdfcreator_get_google_fonts_arr[$sf_key])){
                                    foreach ($arm_pdfcreator_get_google_fonts_arr[$sf_key] as $arm_pdfcreator_google_fonts_data) {
                                        if(count($arm_pdfcreator_google_fonts_data)){
                                            $arm_pdf_ttf_files_arr=array();
                                            foreach ($arm_pdfcreator_google_fonts_data as $skey => $sfile_url) {
                                               if (filter_var($sfile_url, FILTER_VALIDATE_URL)) {
                                                    $fname_font = basename($sfile_url);
                                                    $arm_pdf_ttf_files_arr[$skey]=$fname_font;
                                               }else{
                                                    $arm_pdf_ttf_files_arr[$skey]=$sfile_url;
                                               } 
                                            }
                                            $fontData[$sf_key]=$arm_pdf_ttf_files_arr;
                                        }
                                    }        
                                }
                            }    
                        }
                    }
                } 
                if(count($fontData)>0){
                    foreach ($fontData as $fdata_key => $fdata_value) {
                        if(count($fdata_value)>0){
                            foreach ($fdata_value as $key => $font_file_name) {
                               if (filter_var($font_file_name, FILTER_VALIDATE_URL)) {
                                    $fname_font = basename($font_file_name);
                                    $font_file_path=ARMPDFCREATOR_FONT_DIR.'/'.$fname_font; 
                               }else{
                                    $font_file_path=ARMPDFCREATOR_FONT_DIR.'/'.$font_file_name; 
                               } 
                               
                               $ext  = pathinfo($font_file_path, PATHINFO_EXTENSION);
                               if(@!file_exists($font_file_path) && @empty($ext)){
                                    unset($fontData[$fdata_key][$key]);
                               }
                            }
                            
                        }

                    }
                }

                $arm_pdf_constructor = array(
                'fontDir'           => array_merge(
                    $fontDirs,
                    array(
                        ARMPDFCREATOR_FONT_DIR
                    )
                ),
                'fontdata' => $fontData,
                'mode' => 'utf-8',
                'format' => 'A4',
                'margin_left' => 5,
                'margin_right' => 5,
                'margin_top' => 20,
                'margin_bottom' => 10,
                'margin_header' => 5,
                'margin_footer' => 5,
                'orientation' => 'P',
                'debug' => false,
                'allow_output_buffering' => false
            );
             
            $arm_pdfcreator_mpdf = new Mpdf\Mpdf($arm_pdf_constructor);

            $arm_pdfcreator_mpdf->setAutoTopMargin = 'stretch';
            $arm_pdfcreator_mpdf->setAutoBottomMargin = 'stretch';
            $arm_pdfcreator_mpdf->autoScriptToLang = true;
            $arm_pdfcreator_mpdf->baseScript = 1;
            $arm_pdfcreator_mpdf->autoVietnamese = true;
            $arm_pdfcreator_mpdf->autoArabic = true;
            $arm_pdfcreator_mpdf->autoLangToFont = true;
            if(is_rtl())
            {
                $arm_pdfcreator_mpdf->SetDirectionality('rtl');
            }
            if(is_user_logged_in() && isset($_REQUEST['action']) && $_REQUEST['action']=="arm_pdfcreates"){
                
                try {
                    
                    if(isset($_REQUEST['member_id']) && !empty($_REQUEST['member_id']) && isset($_REQUEST['arm_mcard_id']) && !empty($_REQUEST['arm_mcard_id']) && isset($_REQUEST['iframe_id']) && !empty($_REQUEST['iframe_id']) && isset($_REQUEST['plan_id']) && !empty($_REQUEST['plan_id'])){
                        
                        $arm_card_return_data=arm_pdf_card_html_content($_REQUEST['arm_mcard_id'],$_REQUEST['iframe_id'],$_REQUEST['member_id'],$_REQUEST['plan_id'],'ARMPDF');//phpcs:ignore
                        if(!empty($arm_card_return_data['arm_card_html_view']) && !empty($arm_card_return_data['card_css'])){

                            $arm_card_html_view=$arm_card_return_data['arm_card_html_view'];
                            $card_css=$arm_card_return_data['card_css'];
                            $user_info = get_userdata($_REQUEST['member_id']);//phpcs:ignore
                            $username = $user_info->user_login;
                            $first_name = $user_info->first_name;
                            $last_name = $user_info->last_name;
                            $pdf_template_name=$username;
                            $arm_pdf_title='User Card';
                            if(!empty($first_name) || $last_name){
                                $arm_pdf_title=$first_name.''.$last_name;
                            }else{
                                $arm_pdf_title=$username;
                            }
                            $card_temps_data = $wpdb->get_results("SELECT arm_options FROM `" . $ARMember->tbl_arm_member_templates . "` WHERE arm_id = {$_REQUEST['arm_mcard_id']} AND arm_type = 'arm_card' ", ARRAY_A);//phpcs:ignore
                            
                            if(!empty($card_temps_data)){
                                $card_temps_data = array_column($card_temps_data, "arm_options");
                                $card_data_opts = maybe_unserialize($card_temps_data[0]);    
                                
                                $arm_card_ttl_font_family = !empty($card_data_opts["title_font"]["font_family"]) ? $card_data_opts["title_font"]["font_family"] : "";
                                if(!empty($arm_card_ttl_font_family)){
                                    $arm_card_ttl_font_family_replace=str_replace(' ', '-', strtolower($arm_card_ttl_font_family));
                                    $card_css=str_replace($arm_card_ttl_font_family,$arm_card_ttl_font_family_replace, $card_css);
                                }

                                 $arm_card_lbl_font_family = !empty($card_data_opts["label_font"]["font_family"]) ? $card_data_opts["label_font"]["font_family"] : "";
                                if(!empty($arm_card_lbl_font_family)){
                                    $arm_card_lbl_font_family_replace=str_replace(' ', '-', strtolower($arm_card_lbl_font_family));
                                    $card_css=str_replace($arm_card_lbl_font_family,$arm_card_lbl_font_family_replace, $card_css);
                                }
                                 $arm_card_content_font_family = !empty($card_data_opts["content_font"]["font_family"]) ? $card_data_opts["content_font"]["font_family"] : "";
                                 if(!empty($arm_card_content_font_family)){
                                    $arm_card_content_font_family_replace=str_replace(' ', '-', strtolower($arm_card_content_font_family));
                                    $card_css=str_replace($arm_card_content_font_family,$arm_card_content_font_family_replace, $card_css);
                                }
                            }
                            $arm_pdfcreator_mpdf->WriteHTML($card_css,1);
                            $arm_pdfcreator_mpdf->WriteHTML($arm_card_html_view,2);
                            $arm_pdfcreator_mpdf->SetTitle($arm_pdf_title);

                            $pdffilename    = $pdf_template_name.'.pdf';
                            $arm_pdfcreator_mpdf->Output($pdffilename, 'I');

                            exit;
                        }
                        
                    }else if(isset($_REQUEST['invoice_id']) && !empty($_REQUEST['invoice_id'])){
                        $invoice_css='body {margin: 0;padding: 0;font: 12pt "Tahoma";}.page {width: 700px;min-height: 600px;padding: 20px;margin: 0 auto;background: white;}';
                        $arm_invoice_html_view='';
                        $arm_invoice_tax_feature = get_option('arm_is_invoice_tax_feature', 0);
                        if($arm_invoice_tax_feature) {
                            $log_id=intval($_REQUEST['invoice_id']);
                            if (!empty($log_id) && $log_id != 0) {
                                global $arm_transaction;

                                $log_detail = $arm_transaction->arm_get_single_transaction($log_id);
                                
                                $arm_invoice_html_view .=arm_pdf_invoice_html_content($log_id);      
                                
                                $arm_pdf_title=__('Invoice','ARMPdfCreator').' #'.$log_detail['arm_invoice_id'];
                                $pdf_template_name='invoice_'.$log_detail['arm_invoice_id'];
                                
                                $arm_invoice_html_doc = new DOMDocument();
                                $arm_invoice_html_doc->loadHTML($arm_invoice_html_view);
                                $style_output = $arm_invoice_html_doc->getElementsByTagName("style");
                                if($style_output->length>0){
                                    for ($i=0; $i < $style_output->length; $i++) { 
                                        $invoice_css .=$style_output->item($i)->nodeValue;;
                                    }
                                }
                                $arm_invoice_html_view = preg_replace('/<style\b[^>]*>(.*?)<\/style>/i', '', $arm_invoice_html_view);
                                $arm_invoice_html_view = preg_replace('/<script\b[^>]*>(.*?)<\/script>/i', '', $arm_invoice_html_view);
                                $arm_pdfcreator_mpdf->WriteHTML($invoice_css,1);
                                $arm_pdfcreator_mpdf->WriteHTML($arm_invoice_html_view,2);
                                $arm_pdfcreator_mpdf->SetTitle($arm_pdf_title);
                                $pdffilename    = $pdf_template_name.'.pdf';
                                $arm_pdfcreator_mpdf->Output($pdffilename, 'I');
                                exit;
                            }
                        }   
                    }else if(!empty($_REQUEST['course_id'])){
                        if( is_plugin_active( 'armembercourses/armembercourses.php' ) ) {
                            $course_id=intval($_REQUEST['course_id']);
                            if (!empty($course_id)) {
                                global $arm_courses_settings;
                                $arm_current_user_details = wp_get_current_user();
                                $arm_user_id=$arm_current_user_details->ID;
                                $username = $arm_current_user_details->user_login;
                                $arm_purchased_course = get_user_meta( $arm_user_id, 'arm_user_post_ids', true);
                                if(empty($arm_purchased_course)){
                                    $arm_purchased_course=array();
                                }
                                $arm_course_total_completed_lesson = $arm_courses_settings->arm_get_total_course_completed_lesson($course_id);
                                $arm_total_course_lessons = $arm_courses_settings->arm_get_total_course_lesson($course_id);
                                if(in_array($course_id, $arm_purchased_course) && !empty($arm_total_course_lessons) && $arm_course_total_completed_lesson == $arm_total_course_lessons){
                                    $arm_certificate_html_view='';
                                    $arm_courses_option_settings = $arm_courses_settings->get_arm_courses_settings();
                                    $arm_courses_active_certificate = $arm_courses_option_settings['arm_courses_active_certificate'];
                                    $page_size = ($arm_courses_active_certificate =='certificate3')? 'P':'L';
                                    $certificate_css = '@page {  margin: 42px; }@page .arm_course_certificate{padding:40px;}.arm-course-certificate-02-box .arm-body-sec .arm-body-name::after{display:none;}.arm-course-certificate-03-box .arm-body-sec .arm-body-name::after{display:none;}';

                                    $view_type="ARMPDF";
                                    $arm_certificate_get_content = $arm_courses_settings->arm_course_get_certificate_html($arm_courses_active_certificate, $course_id, $view_type);
                                    $arm_certificate_html_view = $arm_certificate_get_content['arm_courses_certificate_content'];
                                    $certificate_css .= $arm_certificate_get_content['arm_courses_certificate_css'];
                                    
                                    $arm_pdf_title = __('Certificate','ARMPdfCreator');
                                    $pdf_template_name = 'certificate_' . intval($_REQUEST['course_id']) . '_'.$username;

                                    $arm_certificate_html_view = preg_replace('/<style\b[^>]*>(.*?)<\/style>/i', '', $arm_certificate_html_view);
                                    $arm_certificate_html_view = preg_replace('/<script\b[^>]*>(.*?)<\/script>/i', '', $arm_certificate_html_view);
                                    $arm_pdf_format = 'A4';
                                    if($arm_courses_active_certificate =='certificate1'){
                                        $arm_pdf_format = [277, 375];
                                    }

                                    $arm_pdf_constructor['format'] = $arm_pdf_format;
                                    $arm_pdf_constructor['orientation'] = $page_size;
                                    $arm_pdfcreator_mpdf = new Mpdf\Mpdf($arm_pdf_constructor);
                                    
                                    $arm_pdfcreator_mpdf->WriteHTML($certificate_css,1);
                                    $arm_pdfcreator_mpdf->WriteHTML($arm_certificate_html_view,2);
                                    $arm_pdfcreator_mpdf->SetTitle($arm_pdf_title);
                                    $pdffilename = $pdf_template_name.'.pdf';
                                    $arm_pdfcreator_mpdf->Output($pdffilename, 'I');
                                    exit;
                                }
                            }
                        }
                    }

                    
                } catch (Exception $e) {
                    //echo $e->getmessage();
                }
            }        
        }
		function arm_pdfcreator_get_fonts_arr() {
            global $arm_member_forms;
            $font_arr = array(
                'aboriginalsans'        => array( 'AboriginalSansREGULAR' => array('R'=>'AboriginalSansREGULAR.ttf' ) ),
                'abyssinicasil'         => array( 'Abyssinica_SIL' => array('R'=>'Abyssinica_SIL.ttf' ) ),
                'aegean'                => array( 'Aegean' => array('R'=>'Aegean.otf' ) ),
                'aegyptus'              => array( 'Aegyptus' => array('R'=>'Aegyptus.otf' ) ),
                'akkadian'              => array( 'Akkadian' => array('R'=>'Akkadian.otf' ) ),
                'ayar'                  => array( 'Ayar' => array('R'=>'ayar.ttf' ) ),
                'mph2bdamase'           => array( 'Damase' => array('R'=>'damase_v.2.ttf' ) ),
                'daibannasilbook'       => array( 'DBSILBR' => array('R'=>'DBSILBR.ttf' ) ),
                'dejavusans'            => array( 'DejaVuSans' => array('R'=>'DejaVuSans.ttf','B'=>'DejaVuSans-Bold.ttf','I'=>'DejaVuSans-Oblique.ttf','BI'=>'DejaVuSans-BoldOblique.ttf' ) ),
                'dejavusanscondensed'   => array('DejaVuSansCondensed' => array('R'=>'DejaVuSansCondensed.ttf','B'=>'DejaVuSansCondensed-Bold.ttf','I'=>'DejaVuSansCondensed-Oblique.ttf','BI'=>'DejaVuSansCondensed-BoldOblique.ttf' ) ),
                'dejavusansmono'        => array('DejaVuSansMono' => array('R'=>'DejaVuSansMono.ttf','B'=>'DejaVuSansMono-Bold.ttf','I'=>'DejaVuSansMono-Oblique.ttf','BI'=>'DejaVuSansMono-BoldOblique.ttf' ) ),
                'dejavuserifcondensed'  => array( 'DejaVuSerifCondensed' => array('R'=>'DejaVuSerifCondensed.ttf', 'B'=>'DejaVuSerifCondensed-Bold.ttf', 'I'=>'DejaVuSerifCondensed-Italic.ttf', 'BI'=>'DejaVuSerifCondensed-BoldItalic.ttf' ) ),
                'dhyana'                => array( 'Dhyana' => array('R'=>'Dhyana-Regular.ttf','B'=>'Dhyana-Bold.ttf' ) ),
                'freemono'              => array( 'FreeMono' => array('R'=>'FreeMono.ttf','B'=>'FreeMonoBold.ttf','I'=>'FreeMonoOblique.ttf','BI'=>'FreeMonoBoldOblique.ttf' ) ),
                'freesans'              => array( 'FreeSans' => array('R'=> 'FreeSans.ttf', 'B'=>'FreeSansBold.ttf','I'=> 'FreeSansOblique.ttf','BI'=> 'FreeSansBoldOblique.ttf' ) ),
                'freeserif'             => array( 'FreeSerif' => array('R'=> 'FreeSerif.ttf','B'=> 'FreeSerifBold.ttf','I'=> 'FreeSerifItalic.ttf', 'BI'=>'FreeSerifBoldItalic.ttf' ) ),
                'garuda'                => array( 'Garuda' => array('R'=> 'Garuda.ttf','B'=> 'Garuda-Bold.ttf', 'I'=>'Garuda-Oblique.ttf', 'BI'=>'Garuda-BoldOblique.ttf' ) ),
                'jomolhari'             => array( 'Jomolhari' => array('R'=> 'Jomolhari.ttf' ) ),
                'kaputaunicode'         => array( 'kaputaunicode' => array('R'=> 'kaputaunicode.ttf' ) ),
                'khmeros'               => array( 'KhmerOS' => array('R'=> 'KhmerOS.ttf' ) ),
                'lannaalif'             => array( 'lannaalif' => array( 'R'=>'lannaalif-v1-03.ttf' ) ),
                'lateef'                => array( 'LateefRegOT' => array( 'R'=>'LateefRegOT.ttf' ) ),
                'lohitkannada'          => array( 'Lohit-Kannada' => array( 'R'=>'Lohit-Kannada.ttf' ) ),
                'ocrb'                  => array( 'ocrb' => array('R'=> 'ocrb10.ttf' ) ),
                'padaukbook'            => array( 'Padauk-book' => array( 'R'=>'Padauk-book.ttf' ) ),
                'pothana2000'           => array( 'Pothana2000' => array('R'=> 'Pothana2000.ttf' ) ),
                'quivira'               => array( 'Quivira' => array('R'=> 'Quivira.otf' ) ),
                'sundaneseunicode'      => array( 'SundaneseUnicode' => array( 'R'=>'SundaneseUnicode-1.0.5.ttf' ) ),
                'sun-exta'              => array( 'Sun-ExtA' => array('R'=> 'Sun-ExtA.ttf' ) ),
                'sun-extb'              => array( 'Sun-ExtB' => array('R'=> 'Sun-ExtB.ttf' ) ),
                'estrangeloedessa'      => array( 'SyrCOMEdessa' => array('R'=> 'SyrCOMEdessa.otf' ) ),
                'taameydavidclm'        => array( 'TaameyDavidCLM-Medium' => array('R'=> 'TaameyDavidCLM-Medium.ttf' ) ),
                'taiheritagepro'        => array( 'TaiHeritagePro' => array( 'R'=>'TaiHeritagePro.ttf' ) ),
                'tharlon'               => array( 'Tharlon' => array('R'=> 'Tharlon-Regular.ttf' ) ),
                'unbatang'              => array( 'UnBatang_0613' => array('R'=> 'UnBatang_0613.ttf' ) ),
                'kfgqpcuthmantahanaskh' => array( 'Uthman' => array('R'=> 'Uthman.otf' ) ),
                'xbriyaz'               => array( 'XB Riyaz' => array('R'=> 'XB Riyaz.ttf','B'=> 'XB RiyazBd.ttf','I'=> 'XB RiyazIt.ttf','BI'=> 'XB RiyazBdIt.ttf' ) ),
                'zawgyi-one'            => array( 'ZawgyiOne' => array('R'=> 'ZawgyiOne.ttf' ) ),
            );
            return $font_arr;
        }
        function google_api_fonts(){
            global $arm_member_forms;
            $font_arr=array();
            $all_arm_google_fonts=$arm_member_forms->arm_google_fonts_list();
            $arm_pdfcreator_google_fonts_data=get_option('arm_pdfcreator_google_fonts_data','');
            if(!empty($arm_pdfcreator_google_fonts_data)){

                if(count($arm_pdfcreator_google_fonts_data)>0){
                   foreach ($arm_pdfcreator_google_fonts_data as $fkey => $font_data) {
                        if(in_array($font_data['family'],$all_arm_google_fonts)){
                            
                            $pdf_font_name=str_replace(' ', '-', strtolower($font_data['family']));
                            
                            $pdf_font_files_arr=array();
                            if(isset($font_data['files']['regular'])){
                                $pdf_font_files_arr['R']=$font_data['files']['regular'];
                            }
                            if(isset($font_data['files']['500'])){
                                $pdf_font_files_arr['B']=$font_data['files']['500'];
                            }
                            if(isset($font_data['files']['italic'])){
                                $pdf_font_files_arr['I']=$font_data['files']['italic'];
                            }
                            if(count($pdf_font_files_arr)){
                                $font_arr[$pdf_font_name]=array($font_data['family']=>$pdf_font_files_arr);
                            }    
                        }    
                    }
                }
            }    
            return $font_arr;
        }
        function arm_pdfcreator_fonts_category(){
            $font_categories = array(
                'aboriginalsans'        => 'Cree, Canadian, Aboriginal, Inuktuit',
                'abyssinicasil'         => 'Ethiopic',
                'aegean'                => 'Carian Lycian, Lydian, Phoenecian, Ugaritic, Linear B Old, Italic',
                'aegyptus'              => 'Egyptian, Hieroglyphs',
                'akkadian'              => 'Cuneiforn',
                'ayar'                  => 'Myanmar',
                'mph2bdamase'           => 'Glagolitic, Shavian, Osmanya, Kharoshti, Deserti',
                'daibannasilbook'       => 'New Tai Lue',
                'dejavusans'            => 'Generic',
                'dejavusanscondensed'   => 'Generic',
                'dejavusansmono'        => 'Generic',
                'dejavuserif'           => 'Generic',
                'dejavuserifcondensed'  => 'Generic',
                'dhyana'                => 'Lao',
                'freemono'              => 'Generic',
                'freesans'              => 'Generic',
                'freeserif'             => 'Generic',
                'garuda'                => 'Thai',
                'jomolhari'             => 'Tibetan',
                'kaputaunicode'         => 'Sinhala',
                'khmeros'               => 'Khmer',
                'lannaalif'             => 'Tai Tham',
                'lateef'                => 'Sindhi',
                'lohitkannada'          => 'Kannada',
                'ocrb'                  => 'Generic',
                'padaukbook'            => 'Myanmar',
                'pothana2000'           => 'Telugu',
                'quivira'               => 'Coptic Buhid, Tagalog, Tagbanwa, Lisu',
                'sundaneseunicode'      => 'Sundanese',
                'sun-exta'              => 'Chinese, Japanese, Runic',
                'sun-extb'              => 'Chinese, Japanese, Runic',
                'estrangeloedessa'      => 'Syriac',
                'taameydavidclm'        => 'Hebrew',
                'taiheritagepro'        => 'Tai Viet',
                'tharlon'               => 'Myanmar Tai Le',
                'unbatang'              => 'Korean',
                'kfgqpcuthmantahanaskh' => 'Arabic',
                'xbriyaz'               => 'Arabic',
                'zawgyi-one'            => 'Myanmar'
            );

            return $font_categories;
        }
    }

}
global $arm_pdfcreator_class;
$arm_pdfcreator_class = new ARM_PDFCreator();
//if(!empty($arm_pdfcreator_class->armpdfcreator_license_status())){
    if (file_exists(ARM_PDF_CREATOR_LIB_DIR . '/mpdf/vendor/autoload.php') && $arm_pdfcreator_class->arm_pdfcreator_is_compatible() ) {
        require_once(ARM_PDF_CREATOR_LIB_DIR . '/mpdf/vendor/autoload.php');
    }
    if (file_exists(ARM_PDF_CREATOR_CLASSES_DIR . '/class.arm_pdfcreator_general_settings.php') && $arm_pdfcreator_class->arm_pdfcreator_is_compatible() ) {
        require_once(ARM_PDF_CREATOR_CLASSES_DIR . '/class.arm_pdfcreator_general_settings.php');
    }
//}    

function arm_pdf_invoice_html_content($log_id){
    global $wp, $wpdb, $current_user, $arm_errors, $ARMember, $arm_global_settings, $arm_subscription_plans, $arm_payment_gateways, $global_currency_sym, $arm_transaction, $arm_member_forms,$arm_pdfcreator_class,$arm_members_class,$arm_version;
    
    $arm_invoice_html_view ='<div class="arm_invoice_detail_popup arm_invoice_detail_popup_wrapper page">';
        $arm_invoice_html_view .='<div class="popup_wrapper_inner" style="overflow: hidden;">';
            $arm_invoice_html_view .='<div class="popup_content_text arm_invoice_detail_popup_text" id="arm_invoice_detail_popup_text">';
            
            $date_time_format = $arm_global_settings->arm_get_wp_date_time_format();
            $log_type = '';
            /* Get Edit Rule Form HTML */
            if (!empty($log_id) && $log_id != 0) {
                
                $log_detail = $arm_transaction->arm_get_single_transaction($log_id);
                
                ob_start(); // start capturing output.
                do_action('arm_before_displaying_invoice_content');
                $arm_invoice_html_view .= ob_get_contents(); 
                ob_end_clean(); 
                
                $all_global_settings = $arm_global_settings->arm_get_all_global_settings();
                $all_general_settings = isset($all_global_settings['general_settings']) ? $all_global_settings['general_settings'] : array();
                $all_currencies = $arm_payment_gateways->arm_get_all_currencies();
                if (!empty($all_general_settings)) {
                    $content = $all_global_settings['general_settings']['arm_invoice_template'];
                    $content = apply_filters('arm_get_modified_locale_invoice_template_externally',$content);
                    if (!empty($content)) {
                    
                        $user_info = get_userdata($log_detail['arm_user_id']);
                        $user_first_name = get_user_meta($log_detail['arm_user_id'], 'first_name', true);
                        $user_last_name = get_user_meta($log_detail['arm_user_id'], 'last_name', true);
                        $user_plan_data = get_user_meta($log_detail['arm_user_id'], 'arm_user_plan_' . $log_detail['arm_plan_id'], true);
                        $plan_detail = (isset($user_plan_data['arm_current_plan_detail']) && !empty($user_plan_data['arm_current_plan_detail'])) ? $user_plan_data['arm_current_plan_detail'] : array();
                        $plan_detail = apply_filters('arm_modify_plan_info_invoice_external_use',$plan_detail);

                        $arm_log_added_date = $log_detail['arm_created_date'];
                      
                        if (!empty($plan_detail)) {
                        $curPlan = new ARM_Plan(0);
                            $curPlan->init((object) $plan_detail);
                        } else {
                            $curPlan = new ARM_Plan($log_detail['arm_plan_id']);
                        }
                        

                        if ($log_detail['arm_payment_gateway'] == '') {
                            $i_payment_gateway = __('Manual', 'ARMPdfCreator');
                        } else {
                            $i_payment_gateway = $arm_payment_gateways->arm_gateway_name_by_key($log_detail['arm_payment_gateway']);
                        }

                        $payment_cycle = isset($user_plan_data['arm_payment_cycle']) ? $user_plan_data['arm_payment_cycle'] : 0;
                        $i_plan_description = $curPlan->description;

                        $global_currency = $arm_payment_gateways->arm_get_global_currency();
                        $t_currency = (isset($log_detail['arm_currency']) && !empty($log_detail['arm_currency'])) ? strtoupper($log_detail['arm_currency']) : strtoupper($global_currency);
                        $currency = (isset($all_currencies[$t_currency])) ? $all_currencies[$t_currency] : $global_currency_sym;
                        $transAmount = '';
                        $extraVars = (!empty($log_detail['arm_extra_vars'])) ? maybe_unserialize($log_detail['arm_extra_vars']) : array();

                        if (!empty($extraVars) && !empty($extraVars['plan_amount']) && $extraVars['plan_amount'] != 0 && $extraVars['plan_amount'] != $log_detail['arm_amount']) {
                            $transAmount .= '<span class="arm_transaction_list_plan_amount">' . $arm_payment_gateways->arm_prepare_amount($t_currency, $extraVars['plan_amount']) . '</span>';
                        }
                        $transAmount .= '<span class="arm_transaction_list_paid_amount">';
                        if (!empty($log_detail['arm_amount']) && $log_detail['arm_amount'] > 0) {
                            $transAmount .= $arm_payment_gateways->arm_prepare_amount($t_currency, $log_detail['arm_amount']);
                            if ($global_currency_sym == $currency && strtoupper($global_currency) != $t_currency) {
                                $transAmount .= ' (' . $t_currency . ')';
                            }
                        } else {
                            $transAmount .= $arm_payment_gateways->arm_prepare_amount($t_currency, $log_detail['arm_amount']);
                        }
                        $transAmount .= '</span>';
                        $transAmount = $arm_payment_gateways->arm_prepare_amount($t_currency, $log_detail['arm_amount']);

                        $trialInterval = '';
                        if (!empty($extraVars) && isset($extraVars['trial'])) {
                            $trialInterval = $extraVars['trial']['interval'] . " ";

                            if ($extraVars['trial']['period'] == 'Y') {
                                $trialInterval .= ($trialInterval > 1) ? __('Years', 'ARMPdfCreator') : __('Year', 'ARMPdfCreator');
                            } elseif ($extraVars['trial']['period'] == 'M') {
                                $trialInterval .= ($trialInterval > 1) ? __('Months', 'ARMPdfCreator') : __('Month', 'ARMPdfCreator');
                            } elseif ($extraVars['trial']['period'] == 'W') {
                                $trialInterval .= ($trialInterval > 1) ? __('Weeks', 'ARMPdfCreator') : __('Week', 'ARMPdfCreator');
                            } elseif ($extraVars['trial']['period'] == 'D') {
                                $trialInterval .= ($trialInterval > 1) ? __('Days', 'ARMPdfCreator') : __('Day', 'ARMPdfCreator');
                            }
                        }
                        $arm_trial_amount = isset($extraVars['trial']['amount']) ? $extraVars['trial']['amount'] : 0;

                        $arm_tax_amount = '-';
                        if(!empty($extraVars) && isset($extraVars['tax_amount'])){
                            $arm_tax_amount = ($extraVars['tax_amount']!='') ? $arm_payment_gateways->arm_prepare_amount($t_currency, $extraVars['tax_amount']): '-';
                           
                        }

                        $arm_tax_percentage = '-';
                        if(!empty($extraVars) && isset($extraVars['tax_percentage'])){
                            $arm_tax_percentage = ($extraVars['tax_percentage']!='') ? $extraVars['tax_percentage'].'%': '-';
                           
                        }


                        $arm_used_coupon_discount = '';
                        if (!empty($log_detail['arm_coupon_code'])) {
                            if (!empty($log_detail['arm_coupon_discount']) && $log_detail['arm_coupon_discount'] > 0) {
                                $arm_used_coupon_discount = number_format((float) $log_detail['arm_coupon_discount'], 2);
                                $arm_used_coupon_discount.= ($log_detail['arm_coupon_discount_type'] != 'percentage') ? " " . $log_detail['arm_coupon_discount_type'] : "%";
                            } else {
                                $arm_used_coupon_discount = 0;
                            }
                        } else {
                            $arm_used_coupon_discount = '-';
                        };
                        
                        
                        $user_plan_name = $arm_subscription_plans->arm_get_plan_name_by_id($log_detail['arm_plan_id']);
                        $user_plan_name = apply_filters('arm_modify_plan_name_external_use',$user_plan_name,$log_detail['arm_plan_id']);
                        
                        $payer_email = '';
                        if($log_detail['arm_payer_email'] == '')
                        {
                            $extra = maybe_unserialize($log_detail['arm_extra_vars']);
                            if($extra != '')
                            {
                            if(array_key_exists('manual_by',$extra)){

                                $payer_email = '<em>' . $extra['manual_by'] . '</em>'; //phpcs:ignore
                            }
                            }
                        }
                        else
                        {
                            $payer_email = $log_detail['arm_payer_email'];
                        }

                        $date_format = $arm_global_settings->arm_get_wp_date_format();

                        $historyRecords = $wpdb->get_row( $wpdb->prepare("SELECT * FROM ".$ARMember->tbl_arm_activity." WHERE arm_user_id=%d AND arm_item_id = %d AND arm_action != %s AND arm_date_recorded <= %s ORDER BY arm_activity_id DESC",$log_detail['arm_user_id'],$log_detail['arm_plan_id'],'recurring_subscription',$arm_log_added_date), ARRAY_A);

                        $historyContent = !empty($historyRecords['arm_content']) ? maybe_unserialize($historyRecords['arm_content']) : '';

                        $arm_paid_date = !empty($historyRecords['arm_date_recorded']) ? strtotime($historyRecords['arm_date_recorded']) : '';

                        $arm_start_plan_date = !empty($historyRecords['arm_activity_plan_start_date']) ? date_i18n($date_format, strtotime($historyRecords['arm_activity_plan_start_date'])) : '';

                        $arm_sub_plan_type = ( is_array( $historyContent ) && isset( $historyContent['plan_detail'] ) && isset( $historyContent['plan_detail']['arm_subscription_plan_type'] ) ) ? $historyContent['plan_detail']['arm_subscription_plan_type'] : '';

                        //get payment count from start date to payment log date with user id and plan ID
                       
                        $arm_end_plan_date = (!empty($historyRecords['arm_activity_plan_end_date']) &&  $historyRecords['arm_activity_plan_end_date'] != '0000-00-00 00:00:00') ? date_i18n($date_format, strtotime($historyRecords['arm_activity_plan_end_date'])) : esc_html__('Never', 'ARMPdfCreator');

                        $arm_sub_end_plan_date = (!empty($historyRecords['arm_activity_plan_next_cycle_date'])  &&  $historyRecords['arm_activity_plan_next_cycle_date'] != '0000-00-00 00:00:00' ) ? date_i18n($date_format, strtotime($historyRecords['arm_activity_plan_next_cycle_date'])) : esc_html__('Never', 'ARMPdfCreator');

                        if($arm_sub_plan_type == 'recurring' && str_contains( $content,'{ARM_SUBSCRIPTION_END_DATE}') && version_compare($arm_version, '6.8', '>='))
                        {
                            $historyRecordData = $wpdb->get_results( $wpdb->prepare("SELECT arm_log_id FROM ".$ARMember->tbl_arm_payment_log." WHERE arm_log_id <=%d AND arm_user_id=%d AND arm_plan_id = %d AND arm_created_date >= %s ",$log_detail['arm_log_id'],$log_detail['arm_user_id'],$log_detail['arm_plan_id'],$historyRecords['arm_activity_plan_start_date']), ARRAY_A);

                            if(count($historyRecordData) == 1)
                            {
                                $arm_sub_end_plan_date = (!empty($historyRecords['arm_activity_plan_next_cycle_date'])  &&  $historyRecords['arm_activity_plan_next_cycle_date'] != '0000-00-00 00:00:00' ) ? date_i18n($date_format, strtotime($historyRecords['arm_activity_plan_next_cycle_date'])) : esc_html__('Never', 'ARMPdfCreator');
                            }
                            else
                            {
                                $user_id = $log_detail['arm_user_id'];
                                $planID = $log_detail['arm_plan_id'];
                                $num_rec = count($historyRecordData);
                                $planStart = strtotime($arm_start_plan_date);
                            
                                $arm_sub_end_plan_date = $arm_members_class->arm_get_next_due_date_by_start_date($user_id,$planID,$planStart,$payment_cycle,$num_rec);

                                $arm_sub_end_plan_date = date_i18n($date_format, $arm_sub_end_plan_date);
                            }
                        }

                        $content = str_replace('{ARM_PLAN_START_DATE}', $arm_start_plan_date, $content);
                        $content = str_replace('{ARM_PLAN_END_DATE}', $arm_end_plan_date, $content);
                        
                        
                        $arm_used_coupon_code = (!empty($log_detail['arm_coupon_code'])) ? $log_detail['arm_coupon_code'] : '-';
                        $content = str_replace('{ARM_INVOICE_USERNAME}', $user_info->user_login, $content);
                        $content = str_replace('{ARM_INVOICE_USERFIRSTNAME}', $user_first_name, $content);
                        $content = str_replace('{ARM_INVOICE_USERLASTNAME}', $user_last_name, $content);
                        $content = str_replace('{ARM_INVOICE_SUBSCRIPTIONNAME}', $user_plan_name, $content);
                        $content = str_replace('{ARM_INVOICE_SUBSCRIPTIONDESCRIPTION}', $i_plan_description, $content);
                        $content = str_replace('{ARM_INVOICE_GATEWAY}', $i_payment_gateway, $content);
                        $content = str_replace('{ARM_INVOICE_TRANSACTIONID}', $log_detail['arm_transaction_id'], $content);

                        $arm_invoice_id = $arm_global_settings->arm_manipulate_invoice_id($log_detail['arm_invoice_id']);

                        $content = str_replace('{ARM_INVOICE_INVOICEID}', $arm_invoice_id, $content);

                        $content = str_replace('{ARM_INVOICE_SUBSCRIPTIONID}', $log_detail['arm_token'], $content);
                        $content = str_replace('{ARM_INVOICE_AMOUNT}', $transAmount, $content);
                        $content = str_replace('{ARM_INVOICE_PAYMENTDATE}', date_i18n($date_time_format, strtotime($log_detail['arm_created_date'])), $content);
                        $content = str_replace('{ARM_INVOICE_PAYEREMAIL}', $payer_email, $content);
                        $content = str_replace('{ARM_INVOICE_TRIALAMOUNT}', $arm_payment_gateways->arm_prepare_amount($t_currency, $arm_trial_amount), $content);
                        $content = str_replace('{ARM_INVOICE_TRIALPERIOD}', $trialInterval, $content);
                        $content = str_replace('{ARM_INVOICE_COUPONCODE}', $arm_used_coupon_code, $content);
                        $content = str_replace('{ARM_INVOICE_COUPONAMOUNT}', $arm_used_coupon_discount, $content);
                        $content = str_replace('{ARM_INVOICE_TAXPERCENTAGE}', $arm_tax_percentage, $content);
                        $content = str_replace('{ARM_INVOICE_TAXAMOUNT}', $arm_tax_amount, $content);
                        $content = str_replace('{ARM_SUBSCRIPTION_START_DATE}', $arm_start_plan_date, $content);
                        $content = str_replace('{ARM_SUBSCRIPTION_END_DATE}', $arm_sub_end_plan_date, $content);

                        $arm_subscription_amount = 0;
                        if(!empty($log_detail['arm_amount']) && $log_detail['arm_amount']>0)
                        {
                            $arm_tax_amount_check = !empty($extraVars['tax_amount']) ? $extraVars['tax_amount'] : 0;
                            $arm_amount_check = !empty($log_detail['arm_amount']) ? $log_detail['arm_amount'] : 0;
                            $arm_subscription_amount = $arm_amount_check-$arm_tax_amount_check;
                            if($arm_subscription_amount<0)
                            {
                                $arm_subscription_amount = 0;
                            }
                        }
                        $arm_subscription_amount = $arm_payment_gateways->arm_prepare_amount($t_currency, $arm_subscription_amount);
                        $content = str_replace('{ARM_INVOICE_SUBSCRIPTIONAMOUNT}', $arm_subscription_amount, $content);

                        $dbFormFields = $arm_member_forms->arm_get_db_form_fields(true);
                        foreach ($dbFormFields as $meta_key => $field) {

                            $field_options = maybe_unserialize($field);
                            $field_options = apply_filters('arm_change_field_options', $field_options);
                            $exclude_keys = array (
                                'first_name', 'last_name', 'user_login', 'user_email', 'user_pass', 'repeat_pass',
                                'arm_user_plan', 'arm_last_login_ip', 'arm_last_login_date', 'roles', 'section',
                                'repeat_pass', 'repeat_email', 'social_fields', 'avatar', 'profile_cover', 'user_pass_', 'display_name', 'description',
                            );
                            $meta_key = isset($field_options['meta_key']) ? $field_options['meta_key'] : $field_options['id'];
                            
                            $type = isset($field_options['type']) ? $field_options['type'] : array();
                               
                            if (!in_array($meta_key, $exclude_keys) && !in_array($type, array('section', 'roles', 'html', 'hidden', 'submit', 'repeat_pass', 'repeat_email'))) {

                                $custom_field_shortcode_pattern = '{ARM_INVOICE_'.trim($meta_key).'}';
                                $custom_field_value = get_user_meta($log_detail['arm_user_id'], trim($meta_key), true);
                                if(is_array($custom_field_value))
                                {
                                    $custom_field_value = implode(',', $custom_field_value);
                                }
                                $content = str_replace($custom_field_shortcode_pattern, $custom_field_value, $content);
                            }
                        }

                    }
                    $content = apply_filters('arm_after_display_invoice_content', $content, $log_detail, $log_id, $log_type);

                    $arm_invoice_html_view .=stripslashes($content);
                }
                ob_start(); // start capturing output.
                do_action('arm_after_displaying_invoice_content');
                $arm_invoice_html_view .= ob_get_contents(); 
                ob_end_clean(); 
                
            }
            $arm_invoice_html_view .='</div>';
        $arm_invoice_html_view .='</div>';
    $arm_invoice_html_view .='</div>';

    $re_font_family = '/font-family.+?;/m';
    $match_font_family=array();
    if(!empty($arm_invoice_html_view)){
        preg_match_all($re_font_family, $arm_invoice_html_view, $match_font_family, PREG_SET_ORDER, 0);
    }    
    if(count($match_font_family)>0){
        $arm_pdfcreator_extra_fonts = get_option('arm_pdfcreator_extra_fonts', '');
        $arm_pdfcreator_get_google_fonts_arr=$arm_pdfcreator_class->google_api_fonts();
        if(!empty($arm_pdfcreator_extra_fonts)){
            if(count($arm_pdfcreator_extra_fonts)>0){
                foreach ($arm_pdfcreator_extra_fonts as $sf_key) {
                    if(count($arm_pdfcreator_get_google_fonts_arr)>0){
                        if(isset($arm_pdfcreator_get_google_fonts_arr[$sf_key])){
                           foreach ($arm_pdfcreator_get_google_fonts_arr[$sf_key] as $key_name => $font_data_value) {
                               $arm_invoice_content_font_family_replace=str_replace(' ', '-', strtolower($key_name));   
                               foreach ($match_font_family as $mkey => $mvalue) {
                                    if(!empty($mvalue['0'])){
                                        $matched_replace=str_replace($key_name,$arm_invoice_content_font_family_replace, $mvalue['0']);
                                        $arm_invoice_html_view=str_replace($mvalue['0'],$matched_replace,$arm_invoice_html_view);
                                    }    
                                }
                           }                                                        
                        }
                    }    
                }
            }
        } 
    }

    return $arm_invoice_html_view;
}
function arm_pdf_card_html_content($arm_mcard_id,$iframe_id,$member_id,$plan_id,$view_type){
    $arm_card_html_view_output='';
    $card_css_output='';
    if(!empty($arm_mcard_id) && !empty($iframe_id) && !empty($member_id) && !empty($plan_id) && !empty($view_type)){
        $card_view_src =  MEMBERSHIP_VIEWS_DIR."/arm_membership_card_template.php";

        $_REQUEST['arm_mcard_id']=$arm_mcard_id;
        $_REQUEST['iframe_id']=$iframe_id;
        $_REQUEST['member_id']=$member_id;
        $_REQUEST['view_type']=$view_type;
        $_REQUEST['plan_id']=$plan_id;
        include($card_view_src);
        $arm_card_html_view_output .=$arm_card_html_view;
        $card_css_output .=$card_css;
    }    
    return array('arm_card_html_view'=>$arm_card_html_view_output,'card_css'=>$card_css_output);
}
add_filter('arm_addon_license_content_external', 'armpdfcreator_addon_license_content_external', 10, 1);
function armpdfcreator_addon_license_content_external($arm_license_addon_content){
    $armpdfcreator_license_status=get_option('armpdfcreator_license_status');
    $armpdfcreator_license_key=get_option('armpdfcreator_license_key');
    $arm_license_deactive=0;
    $arm_license_active_style='display:inline';
    $arm_license_deactive_style='display:none;';
    if($armpdfcreator_license_status=='valid' && !empty($armpdfcreator_license_key)){
        $arm_license_deactive=1;
        $arm_license_active_style='display:none';
        $arm_license_deactive_style='display:inline';        
    }    
    $arm_license_addon_content .='<form method="post" action="#" id="armpdfcreator_license_settings" class="arm_license_settings arm_admin_form" onsubmit="return false;">
        <div class="arm_feature_settings_container arm_feature_settings_wrapper" style="margin-top:30px;">
            <div class="page_sub_title">'.__('Activate PDF Creator Addon','ARMPdfCreator').'</div>            
            <table class="form-table">
                <tr class="form-field">
                    <th class="arm-form-table-label">'.__('License Code', 'ARMPdfCreator').'</th>
                    <td class="arm-form-table-content">';
                    
                        $arm_license_addon_content .='<div id="licenseactivatedmessage" class="armpdfcreator_remove_license_section" style="width:300px; vertical-align:top;padding:5px;'.$arm_license_deactive_style.'">'.$armpdfcreator_license_key.'</div>';
                    
                        $arm_license_addon_content .='<div class="armpdfcreator_add_license_section" style="'.$arm_license_active_style.'"><input type="text" name="arm_license_key" id="arm_license_key" placeholder="Enter License Code" value="'.$armpdfcreator_license_key.'" autocomplete="off" /><div class="arperrmessage" id="arm_license_key_error" style="display:none;">'.__('This field cannot be blank.', 'ARMPdfCreator').'</div></div>'; 

                    $arm_license_addon_content .='</td>
                </tr>
                <tr class="form-field">
                    <th class="arm-form-table-label">&nbsp;</th>
                    <td class="arm-form-table-content">';
                    $arm_license_addon_content .='<input type="hidden" name="arm_license_deactive" id="arm_license_deactive" value="'.$arm_license_deactive.'" />';
                        $arm_license_addon_content .='<span id="license_link" class="armpdfcreator_remove_license_section" style="'.$arm_license_deactive_style.'"><button type="button" id="armpdfcreator-remove-verify-purchase-code" name="armpdfcreator_remove_license" style="width:170px; border:0px; color:#FFFFFF; height:40px; border-radius:6px;" class="red_remove_license_btn">'.__('Remove License', 'ARMPdfCreator').'</button></span>';

                        $arm_license_addon_content .='<span id="license_link" class="armpdfcreator_add_license_section" style="'.$arm_license_active_style.'"><button type="button" id="armpdfcreator-verify-purchase-code" name="armpdfcreator_activate_license" style="width:180px; border:0px; color:#FFFFFF; height:40px; border-radius:3px;cursor:pointer;line-height: 15px;" class="greensavebtn">'.__('Activate License', 'ARMPdfCreator').'</button></span>';
                    
                    $arm_license_addon_content .='<span id="license_loader" style="display:none;">&nbsp;<img src="'.MEMBERSHIP_IMAGES_URL.'/loading_activation.gif" height="15" /></span>
                        <span id="license_error" style="display:none;">&nbsp;</span>
                        <span id="license_reset" style="display:none;">&nbsp;&nbsp;<a onclick="javascript:return false;" href="#">Click here to submit RESET request</a></span>
                        <span id="license_success" style="display:none;">'.__('License Activated Successfully.', 'ARMPdfCreator').'</span>                        
                    </td>
                </tr>

            </table>
        </div>
    </form>
    <div class="armclear"></div>';    
     
    global $arm_global_settings;
    /* **********./Begin remove License Popup/.********** */
    $arm_remove_license_popup_content = '<span class="arm_confirm_text">'.__("Are you sure you want to Remove this License?",'ARMPdfCreator' );		
    $arm_remove_license_popup_content .= '<input type="hidden" value="" id="armpdfcreator_remove_license_flag"/>';
    $arm_remove_license_popup_title = '<span class="arm_confirm_text">'.__('Remove License', 'ARMPdfCreator').'</span>';		
    
    $arm_remove_license_popup_arg = array(
        'id' => 'armpdfcreator_remove_license_form_message',
        'class' => 'armpdfcreator_remove_license_form_message',
        'title' => $arm_remove_license_popup_title,
        'content' => $arm_remove_license_popup_content,
        'button_id' => 'armpdfcreator_remove_license_ok_btn',
        'button_onclick' => "armpdfcreator_deactivate_license();",
    );
    $arm_license_addon_content .=$arm_global_settings->arm_get_bpopup_html($arm_remove_license_popup_arg);
    /* **********./End remove License Popup/.********** */
		
    return $arm_license_addon_content;
}

add_action('wp_ajax_armpdfcreatoractivatelicense','armpdfcreator_license_update');
function armpdfcreator_license_update(){
    global $wp, $wpdb, $ARMember, $arm_capabilities_global,$arm_slugs;
    
    $response = array('type' => 'error', 'msg' => __('Sorry, Something went wrong. Please try again.', 'ARMPdfCreator'),'arm_license_status'=>0);
    
    /*check the license page capabilities to user */
    $arm_manage_license_key = !isset(($arm_capabilities_global['arm_manage_license']))?$arm_slugs->licensing:$arm_capabilities_global['arm_manage_license'];
    $ARMember->arm_check_user_cap($arm_manage_license_key, '1', '1');

    if(isset($_POST['arm_license_key']) && !empty($_POST['arm_license_key'])){ //phpcs:ignore
        $arm_license_deactive=(isset($_POST['arm_license_deactive']))?$_POST['arm_license_deactive']:'0';//phpcs:ignore
        // activate license for this addon
        $posted_license_key = trim($_POST['arm_license_key']);//phpcs:ignore
        $posted_license_package = '14249';
        $edd_action='activate_license';
        if($arm_license_deactive=='1'){
            $edd_action='deactivate_license';
            $posted_license_key=get_option('armpdfcreator_license_key');
        }

        $api_params = array(
            'edd_action' => $edd_action,
            'license'    => $posted_license_key,
            'item_id'  => $posted_license_package,
            // 'url'        => home_url()
        );

        if($edd_action!='deactivate_license'){
            $api_params['url'] = home_url();
        }

        // Call the custom API.
        $response = wp_remote_post( ARMADDON_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
        $response_return = array();
        $message = "";
        if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
            $message =  ( is_wp_error( $response ) && ! empty( $response->get_error_message() ) ) ? $response->get_error_message() : __( 'An error occurred, please try again.', 'ARMPdfCreator' );
            $response_return['msg']=$message;
            $response_return['arm_license_status']=0;
        } else {
            $license_data = json_decode( wp_remote_retrieve_body( $response ) );
            
            $license_data_string = wp_remote_retrieve_body( $response );
            if ( false === $license_data->success && ! empty( $license_data->error ) ) {
                switch( $license_data->error ) {
                    case 'expired' :
                        $message = sprintf(
                            __( "Your license key expired on %1\$s.", 'ARMPdfCreator'),
                            date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
                        );
                        break;
                    case 'revoked' :
                        $message = __( 'Your license key has been disabled.', 'ARMPdfCreator');
                        break;
                    case 'missing' :
                        $message = __( 'Invalid license.', 'ARMPdfCreator');
                        break;
                    case 'invalid' :
                    case 'site_inactive' :
                        $message = __( 'Your license is not active for this URL.', 'ARMPdfCreator');
                        break;
                    case 'item_name_mismatch' :
                        $message = __('This appears to be an invalid license key for your selected package.', 'ARMPdfCreator');
                        break;
                    case 'invalid_item_id' :
                            $message = __('This appears to be an invalid license key for your selected package.', 'ARMPdfCreator');
                            break;
                    case 'no_activations_left':
                        $message = __( 'Your license key has reached its activation limit.', 'ARMPdfCreator');
                        break;
                    default :
                        $message = __( 'An error occurred, please try again.', 'ARMPdfCreator');
                        break;
                }
                $response_return['msg']=$message;
                $response_return['arm_license_status']=0;

            }else if($license_data->license === "valid"){
                update_option('armpdfcreator_license_key', $posted_license_key );
                update_option('armpdfcreator_license_package', $posted_license_package );
                update_option('armpdfcreator_license_status', $license_data->license );
                update_option('armpdfcreator_license_data_activate_response', $license_data_string );

                $message = "License Activated Successfully.";
                $response_return['type']='success';
                $response_return['msg']=$message;
                $response_return['arm_license_status']=1;
            } else if( ( $license_data->license === "deactivated" || $license_data->license === "failed" ) && $arm_license_deactive=='1' ) {
                delete_option('armpdfcreator_license_key');
                delete_option('armpdfcreator_license_package');
                delete_option('armpdfcreator_license_status');
                delete_option('armpdfcreator_license_data_activate_response');
                
                $message = "License Deactivated Successfully.";
                $response_return['type']='success';
                $response_return['msg']=$message;
                $response_return['arm_license_status']=0;
            }    

        }
        
    }    
    echo json_encode($response_return);
    die();
}

if ( ! class_exists( 'armember_pro_updater' ) ) {
	require_once ARM_PDF_CREATOR_CLASSES_DIR . '/class.armember_pro_plugin_updater.php';
}

function armember_pdfcreator_plugin_updater() {
    global $arm_pdfcreator_version;

	$plugin_slug_for_update = 'armemberpdfcreator/armemberpdfcreator.php';

	// To support auto-updates, this needs to run during the wp_version_check cron job for privileged users.
	$doing_cron = defined( 'DOING_CRON' ) && DOING_CRON;
	if ( ! current_user_can( 'manage_options' ) && ! $doing_cron ) {
		return;
	}

	// retrieve our license key from the DB
	$license_key = trim( get_option( 'armpdfcreator_license_key' ) );
	$package = trim( get_option( 'armpdfcreator_license_package' ) );

	// setup the updater
	$edd_updater = new armember_pro_updater(
		ARMADDON_STORE_URL,
		$plugin_slug_for_update,
		array(
			'version' => $arm_pdfcreator_version,  // current version number
			'license' => $license_key,             // license key (used get_option above to retrieve from DB)
			'item_id' => $package,       // ID of the product
			'author'  => 'Repute Infosystems', // author of this plugin
			'beta'    => false,
		)
	);

}
add_action( 'init', 'armember_pdfcreator_plugin_updater' );
?>