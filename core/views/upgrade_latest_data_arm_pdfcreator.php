<?php 
global $arm_pdfcreator_version;
$arm_pdfcreator_version = '1.3';
update_option('arm_pdfcreator_version', $arm_pdfcreator_version);

$arm_version_updated_date_key = 'arm_pdf_version_updated_date_'.$arm_pdfcreator_version;
$arm_version_updated_date = current_time('mysql');
update_option($arm_version_updated_date_key, $arm_version_updated_date);