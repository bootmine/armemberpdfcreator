<?php
global $arm_member_forms,$arm_pdfcreator_class;
$arm_pdfcreator_extra_fonts = get_option('arm_pdfcreator_extra_fonts', '');
$arm_pdfcreator_google_api_key=get_option('arm_pdfcreator_google_api_key', '');

$font_arr = $arm_pdfcreator_class->arm_pdfcreator_get_fonts_arr();
$font_lang = $arm_pdfcreator_class->arm_pdfcreator_fonts_category();
$arm_pdfcreator_google_fonts_data=$arm_pdfcreator_class->google_api_fonts();

$titleTooltip = __('ARMember PDF Creator allows to use Google Fonts in the PDF document. For that you need to get Google Font API Key from your Google Account. You can get Google Font API Key from by click', 'ARMPdfCreator').' <a href="https://developers.google.com/fonts/docs/developer_api" target="_blank">'.__('here', 'ARMPdfCreator').'.</a>. '. __('For more information, please click','ARMPdfCreator').' <a href="https://developers.google.com/fonts/docs/developer_api" target="_blank">'.__('here', 'ARMPdfCreator').'.</a>.';
?>

<div class="arm_solid_divider"></div>
<div class="page_sub_title"><?php esc_html_e('PDF Font Settings','ARMPdfCreator');?></div>
<table class="form-table">
    <tr class="form-field">
        <th class="arm-form-table-label"><label><?php esc_html_e('Google Font API Key', 'ARMPdfCreator'); ?></label></th>
        <td class="arm-form-table-content">
             <input class="arm_pdfcreator_google_api_key" id="arm_pdfcreator_google_api_key" type="text" name="arm_general_settings[arm_pdfcreator_google_api_key]" value="<?php echo $arm_pdfcreator_google_api_key;?>"><i class="arm_helptip_icon armfa armfa-question-circle" title="<?php echo htmlentities($titleTooltip);?>"></i><?php //phpcs:ignore?>
        </td>
    </tr>
    <tr class="form-field">
        <th class="arm-form-table-label"><label><?php esc_html_e('PDF Fonts', 'ARMPdfCreator'); ?></label></th>
        <td class="arm-form-table-content">
            <select id="arm_pdf_font_family" class="arm_chosen_selectbox" name="arm_general_settings[arm_pdf_font_family][]" data-placeholder="<?php esc_html_e('Select Font(s)..', 'ARMPdfCreator');?>" multiple="multiple">
            <?php
                if (! empty($font_arr) ) {
                    echo '<optgroup label="Default Fonts">';
                    foreach ( $font_arr as $fontkey => $font_val ) {

                        foreach ( $font_val as $key => $value ) {
                            $fcat_lang='';
                            if(isset($font_lang[ $fontkey ])){
                                $fcat_lang=' ('.esc_html( $font_lang[ $fontkey ] ).')';
                            }
                            $selected_font='';
                            if(!empty($arm_pdfcreator_extra_fonts)){
                                $selected_font=(in_array($fontkey,$arm_pdfcreator_extra_fonts)) ? 'selected="selected"' : "";
                            }    
                            ?>

                            <option class="arm_message_selectbox_op" value="<?php echo $fontkey;?>" <?php echo $selected_font;?>><?php echo esc_html($key.$fcat_lang);?></option><?php //phpcs:ignore?>
                            <?php
                        }
                    }
                    echo '</optgroup>';
                } 
                if(!empty($arm_pdfcreator_google_fonts_data)){
                    echo '<optgroup label="Google Fonts">';
                    foreach ( $arm_pdfcreator_google_fonts_data as $fontkey => $font_val ) {

                        foreach ( $font_val as $key => $value ) {
                            $selected_font='';
                            if(!empty($arm_pdfcreator_extra_fonts)){
                                $selected_font=(in_array($fontkey,$arm_pdfcreator_extra_fonts)) ? 'selected="selected"' : "";
                            }    
                            ?>
                            <option class="arm_message_selectbox_op" value="<?php echo $fontkey;?>" <?php echo $selected_font;?>><?php echo esc_html($key);?></option><?php //phpcs:ignore?>
                            <?php
                        }
                    }
                    echo '</optgroup>';    
                }
            ?>
            </select>

        </td>
    </tr>    
</table>
