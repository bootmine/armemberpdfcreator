<?php
if (!class_exists('ARM_PdfCreator_General_Settings')) {

    class ARM_PdfCreator_General_Settings {

        function __construct() {
            global $arm_social_feature;
            if($arm_social_feature->isSocialFeature){
                add_filter('arm_membership_card_details_outside', array($this, 'arm_card_pdfcreator_details_func'), 10, 6);
            }    
            $arm_invoice_tax_feature = get_option('arm_is_invoice_tax_feature', 0);
            if($arm_invoice_tax_feature) {
                add_filter('arm_membership_invoice_details_outside', array($this, 'arm_invoice_pdfcreator_details_func'), 10, 2);
            }
            if( is_plugin_active( 'armembercourses/armembercourses.php' ) ) {
                add_filter('arm_courses_certificate_details_outside', array($this, 'arm_courses_certificate_details_func'), 10, 3);
            }
            add_filter('arm_before_update_global_settings', array($this, 'arm_pdfcreator_update_global_settings'), 10, 2);
            add_action('arm_after_global_settings_html', array($this, 'arm_pdfcreator_add_general_setting'));
            
            add_filter('arm_email_attachment_file_outside', array($this, 'arm_pdfcreator_email_attachment_card_invoice_func'), 10, 1);

            add_filter('arm_automated_email_attachment_file_outside', array($this, 'arm_pdfcreator_automated_email_attachment_card_invoice_func'), 10, 1);

            add_filter('arm_email_template_save_before', array($this, 'arm_pdfcreator_email_attachment_save'), 10, 2);

            add_filter('arm_automated_email_template_save_before', array($this, 'arm_pdfcreator_automated_email_attachment_save'), 10, 2);
            
            add_filter('arm_add_standard_email_template_field_html', array($this, 'arm_pdfcreator_email_attachment_card_invoice_swtich_html'), 10,1);
            add_filter('arm_add_automated_email_template_field_html', array($this, 'arm_pdfcreator_automated_email_attachment_card_invoice_swtich_html'), 10,1);
            add_filter('arm_standard_message_email_attachment', array($this, 'arm_pdfcreator_signup_complete_email_attachments'), 10,4);

            add_filter('arm_automated_message_email_attachment', array($this, 'arm_pdfcreator_auto_message_email_attachments'), 10,4);
            
        }
        function arm_pdfcreator_update_global_settings($new_global_settings, $post_data){
            global $arm_pdfcreator_class;
            if(isset($_POST['arm_general_settings']['arm_pdfcreator_google_api_key']) && !empty($_POST['arm_general_settings']['arm_pdfcreator_google_api_key'])){ //phpcs:ignore
                $arm_pdf_google_font_api_key=$_POST['arm_general_settings']['arm_pdfcreator_google_api_key']; //phpcs:ignore
                $arm_pdfcreator_google_api_key=get_option('arm_pdfcreator_google_api_key');
                if($arm_pdf_google_font_api_key!=$arm_pdfcreator_google_api_key){
                    $google_fonts_response=wp_remote_get('https://www.googleapis.com/webfonts/v1/webfonts?key='.$arm_pdf_google_font_api_key);
                    $google_fonts_response_code = wp_remote_retrieve_response_code( $google_fonts_response );
                    $arm_google_responseBody = wp_remote_retrieve_body( $google_fonts_response );
                    
                    if (is_wp_error($google_fonts_response) || empty($google_fonts_response) || $google_fonts_response_code != '200') {
                        $arm_google_responseBody_arr=json_decode( $arm_google_responseBody,true);
                        $google_fonts_error=(isset($arm_google_responseBody_arr['error']['message']))?$arm_google_responseBody_arr['error']['message']:'Sorry, Something went wrong. Please try again.';
                        $response = array('type' => 'error', 'msg' =>$google_fonts_error);
                        echo json_encode($response);
                        exit;
                    }else{
                        $arm_live_google_fonts_data = json_decode($arm_google_responseBody,true);
                        $google_font_response=array();
                        if(isset($arm_live_google_fonts_data['items'])){
                            foreach ($arm_live_google_fonts_data['items'] as $font_key => $font_item) {
                                $google_font_arr=array();
                                $google_font_arr['family']=$font_item['family'];
                                $google_font_arr['variants']=$font_item['variants'];
                                $google_font_arr['files']=array();
                                if(isset($font_item['files']) && count($font_item['files'])>0){
                                    $font_name=str_replace(' ', '-', strtolower($font_item['family']));
                                    foreach ($font_item['files'] as $ff_key => $file_url) {
                                        $ff_file_name=$font_name.'-'.$ff_key.'.ttf';
                                        if(!empty($file_url)){
                                            $newfile = 'ttfonts/'.$ff_file_name;
                                            $google_font_arr['files'][$ff_key]=$file_url;
                                            
                                        }
                                    }
                                }
                                $google_font_response[]=$google_font_arr;
                            }
                        }    
                        update_option('arm_pdfcreator_google_api_key', $arm_pdf_google_font_api_key);
                        update_option('arm_pdfcreator_google_fonts_data', $google_font_response);    
                    }
                }   
                
            }else{
                update_option('arm_pdfcreator_google_api_key','');
                update_option('arm_pdfcreator_google_fonts_data','');    
            }
            
            $arm_pdf_font_family=array();
            if(isset($_POST['arm_general_settings']['arm_pdf_font_family'])){ //phpcs:ignore
                $arm_pdf_font_family=$_POST['arm_general_settings']['arm_pdf_font_family']; //phpcs:ignore
                $arm_pdfcreator_extra_fonts = get_option('arm_pdfcreator_extra_fonts');
                if(empty($arm_pdfcreator_extra_fonts)){
                    $arm_pdfcreator_extra_fonts=array();   
                }
                $arm_pdfcreator_extra_result = array_diff($arm_pdf_font_family, $arm_pdfcreator_extra_fonts);
                if(count($arm_pdfcreator_extra_result)>0){
                    $sub_font_err_cnt= 0;
                    $child_font= true;
                    $font_list= $arm_pdfcreator_class->arm_pdfcreator_get_fonts_arr();
                    foreach ( $arm_pdfcreator_extra_result as $fontkey) {
                        if (isset( $font_list[$fontkey]) && !in_array($fontkey,$arm_pdfcreator_extra_fonts)){
                            foreach ($font_list[$fontkey] as $font_data_val) {
                                if(count($font_data_val)>0){
                                    $font_all_upload_flag=true;
                                    
                                    foreach ( $font_data_val as $font ) {
                                        $destination = ARMPDFCREATOR_FONT_DIR . '/' . $font;
                                        $source      = 'https://www.arformsplugin.com/arf_pdfcreator_fonts/?action=arf_pdf_import_form&font=' . $font;
                                        
                                        
                                        $args = array(
                                            'timeout' => 50,
                                        );
                                        if(!file_exists($destination)){
                                            $font_response = wp_remote_get( $source, $args );
                                            if ( isset( $font_response['body'] ) && $font_response['body'] != '' ) {
                                                $newfile = fopen( $destination, 'wb' );
                                                if ( $newfile ) {
                                                   fwrite( $newfile, $font_response['body'] );
                                                } else {
                                                    $child_font = false;
                                                    $sub_font_err_cnt++;
                                                    $font_all_upload_flag=false;
                                                }
                                            }else{
                                                $child_font = false;
                                                $font_all_upload_flag=false;
                                            }
                                        }    
                                    } 
                                    if($font_all_upload_flag==true){
                                        $arm_pdfcreator_extra_fonts[]=$fontkey;
                                    }
                                }else{
                                    $child_font = false;
                                    $font_all_upload_flag=false;
                                }
                            }          
                        }
                        
                    }
                    $arm_pdfcreator_google_fonts_data=$arm_pdfcreator_class->google_api_fonts();
                    if(count($arm_pdfcreator_google_fonts_data)>0){
                        foreach ( $arm_pdfcreator_extra_result as $fontkey ) {
                            if (isset( $arm_pdfcreator_google_fonts_data[$fontkey]) && !in_array($fontkey,$arm_pdfcreator_extra_fonts)){
                                foreach ($arm_pdfcreator_google_fonts_data[$fontkey] as $font_data_val) {
                                    if(count($font_data_val)>0){
                                        $font_all_upload_flag=true;
                                        foreach ( $font_data_val as $font ) {
                                            
                                            $fname_font = basename($font);
                                            $destination = ARMPDFCREATOR_FONT_DIR . '/' . $fname_font;
                                            $source      = $font;
                                            $args = array(
                                                'timeout' => 50,
                                            );
                                            if(!file_exists($destination)){
                                                $font_response = wp_remote_get( $source, $args );
                                                if ( isset( $font_response['body'] ) && $font_response['body'] != '' ) {
                                                    $newfile = fopen( $destination, 'wb' );
                                                    if ( $newfile ) {
                                                       fwrite( $newfile, $font_response['body'] );
                                                    } else {
                                                        $child_font = false;
                                                        $sub_font_err_cnt++;
                                                        $font_all_upload_flag=false;
                                                    }
                                                }else{
                                                    $child_font = false;
                                                    $font_all_upload_flag=false;
                                                }
                                            }    
                                        } 
                                        if($font_all_upload_flag==true){
                                            $arm_pdfcreator_extra_fonts[]=$fontkey;
                                        }
                                    }else{
                                        $child_font = false;
                                        $font_all_upload_flag=false;
                                    }
                                }          
                            }
                            
                        }
                    }

                    $arm_final_pdfcreator_fonts = array_intersect($arm_pdf_font_family, $arm_pdfcreator_extra_fonts);
                    
                    update_option('arm_pdfcreator_extra_fonts', $arm_final_pdfcreator_fonts);

                    if($child_font == false && $sub_font_err_cnt > 0){
                        $response = array('type' => 'error', 'msg' => esc_html__( 'Some of the fonts are not imported. Please try again later.', 'ARMPdfCreator'));
                        echo json_encode($response);
                        exit;
                    }                    
                }else{
                    update_option('arm_pdfcreator_extra_fonts', $arm_pdf_font_family);                    
                }
                
            }else{
                update_option('arm_pdfcreator_extra_fonts', $arm_pdf_font_family);
            }    
            
            return $new_global_settings;
        }
        function arm_pdfcreator_add_general_setting() {
            if (file_exists(ARM_PDF_CREATOR_VIEWS_DIR . '/arm_pdfcreator_general_setting.php')) {
                require_once( ARM_PDF_CREATOR_VIEWS_DIR . '/arm_pdfcreator_general_setting.php' );
            }
        }
                
        function arm_card_pdfcreator_details_func($card_pdf_icon_html,$user_id,$arm_mcard_id,$icon_color,$frame_id,$plan_info) {
            $content_html =$card_pdf_icon_html;
            
            $plan_id=(isset($plan_info['arm_current_plan_detail']['arm_subscription_plan_id']))?$plan_info['arm_current_plan_detail']['arm_subscription_plan_id']:'0';
            if(!empty($user_id) && !empty($arm_mcard_id) && !empty($frame_id) && !empty($plan_id)){
                $content_html ='<a class="arm_card_pdf_btn" href="'. site_url().'/?action=arm_pdfcreates&member_id='.$user_id.'&arm_mcard_id='.$arm_mcard_id.'&iframe_id='.$frame_id.'&plan_id='.$plan_id.'" target="_blank">';
                $content_html .= "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='35px' height='35px' viewBox='0 0 960 960'><g id='icomoon-ignore'></g><path fill='".$icon_color."' d='M630 390v-90l-180-210h-329.917c-33.183 0-60.082 26.95-60.082 60.22v689.56c0 33.259 26.728 60.22 59.923 60.22h450.155c33.095 0 59.923-26.947 59.923-59.723v-60.277h239.795c49.819 0 90.205-40.267 90.205-89.963v-210.073c0-49.686-40.080-89.963-90.205-89.963h-239.795zM600 780v60.198c0 16.453-13.431 29.802-29.999 29.802h-450.003c-16.358 0-29.999-13.371-29.999-29.864v-690.273c0-16.206 13.373-29.864 29.867-29.864h300.133v149.823c0 33.581 26.835 60.177 59.937 60.177h120.063v60h-239.795c-49.819 0-90.205 40.267-90.205 89.963v210.073c0 49.686 40.080 89.963 90.205 89.963h239.795zM450 135l140.998 165h-111.097c-16.383 0-29.902-13.55-29.902-30.263v-134.737zM359.837 420h510.326c32.767 0 59.837 26.755 59.837 59.761v210.479c0 32.755-26.79 59.761-59.837 59.761h-510.326c-32.767 0-59.837-26.755-59.837-59.761v-210.479c0-32.755 26.79-59.761 59.837-59.761zM750 570v-60h120v-30h-150v210h30v-90h90v-30h-90zM360 540v-60h89.854c33.329 0 60.146 26.863 60.146 60 0 33.368-26.929 60-60.146 60h-59.854v90h-30v-150zM390 510v60h60.031c16.552 0 29.969-13.316 29.969-30 0-16.568-13.28-30-29.969-30h-60.031zM540 480h89.854c33.329 0 60.146 26.943 60.146 60.177v89.645c0 33.581-26.929 60.177-60.146 60.177h-89.854v-210zM570 510v150h60.031c16.552 0 29.969-13.312 29.969-29.994v-90.011c0-16.566-13.28-29.994-29.969-29.994h-60.031z'></path></svg>";
                $content_html .='</a>';
            }    

            return $content_html;
            
        }
        function arm_invoice_pdfcreator_details_func($invoice_pdf_icon_html,$invoice_id){
            $content_html = $invoice_pdf_icon_html;
            if(!empty($invoice_id)){
                $log_type='';
                $pdf_view_url=site_url().'/?action=arm_pdfcreates&invoice_id='.$invoice_id;
                $content_html ="<button type='button' name='print' onclick=\"window.open('".$pdf_view_url."', '_blank').focus();\" value='Print' class='armemailaddbtn'>".__('View PDF', 'ARMPdfCreator')."</button>";
            }    

            return $content_html;
        }
        function arm_courses_certificate_details_func($certificate_pdf_icon_html, $course_id, $icon_color){
            $content_html = $certificate_pdf_icon_html;
            if(!empty($course_id)){
                $pdf_view_url=site_url().'/?action=arm_pdfcreates&course_id='.$course_id;

                $content_html ='<a class="arm_course_certificate_pdf_btn" href="'. $pdf_view_url.'" target="_blank">';
                $content_html .= "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='35px' height='35px' viewBox='0 0 960 960'><g id='icomoon-ignore'></g><path fill='".$icon_color."' d='M630 390v-90l-180-210h-329.917c-33.183 0-60.082 26.95-60.082 60.22v689.56c0 33.259 26.728 60.22 59.923 60.22h450.155c33.095 0 59.923-26.947 59.923-59.723v-60.277h239.795c49.819 0 90.205-40.267 90.205-89.963v-210.073c0-49.686-40.080-89.963-90.205-89.963h-239.795zM600 780v60.198c0 16.453-13.431 29.802-29.999 29.802h-450.003c-16.358 0-29.999-13.371-29.999-29.864v-690.273c0-16.206 13.373-29.864 29.867-29.864h300.133v149.823c0 33.581 26.835 60.177 59.937 60.177h120.063v60h-239.795c-49.819 0-90.205 40.267-90.205 89.963v210.073c0 49.686 40.080 89.963 90.205 89.963h239.795zM450 135l140.998 165h-111.097c-16.383 0-29.902-13.55-29.902-30.263v-134.737zM359.837 420h510.326c32.767 0 59.837 26.755 59.837 59.761v210.479c0 32.755-26.79 59.761-59.837 59.761h-510.326c-32.767 0-59.837-26.755-59.837-59.761v-210.479c0-32.755 26.79-59.761 59.837-59.761zM750 570v-60h120v-30h-150v210h30v-90h90v-30h-90zM360 540v-60h89.854c33.329 0 60.146 26.863 60.146 60 0 33.368-26.929 60-60.146 60h-59.854v90h-30v-150zM390 510v60h60.031c16.552 0 29.969-13.316 29.969-30 0-16.568-13.28-30-29.969-30h-60.031zM540 480h89.854c33.329 0 60.146 26.943 60.146 60.177v89.645c0 33.581-26.929 60.177-60.146 60.177h-89.854v-210zM570 510v150h60.031c16.552 0 29.969-13.312 29.969-29.994v-90.011c0-16.566-13.28-29.994-29.969-29.994h-60.031z'></path></svg>";
                $content_html .='</a>';
            }    

            return $content_html;
        }
        function arm_pdfcreator_email_attachment_card_invoice_func($arm_email_data){
            global $arm_email_settings, $arm_social_feature,$arm_pdfcreator_attachment_templates;
            
            
            $template_id=$arm_email_data['id'];
            if(!empty($template_id)){
                $arm_email_data['arm_pdfcreator_email_switch']='0';
                $arm_email_data['arm_email_card_attachment_status']='';
                $arm_email_data['arm_email_invoice_attachment_status']='';
                $email_temp_detail = $arm_email_settings->arm_get_single_email_template($template_id,array('arm_template_slug','arm_email_pdf_attachment_status'));
                if(!empty($email_temp_detail)){
                    $arm_email_template_slug=$email_temp_detail->arm_template_slug;
                    if(in_array($arm_email_template_slug, $arm_pdfcreator_attachment_templates)){
                        $arm_email_data['arm_pdfcreator_email_switch']='1';
                    }
                    $arm_email_pdf_attachment_status=$email_temp_detail->arm_email_pdf_attachment_status;
                    if(!empty($arm_email_pdf_attachment_status) && in_array($arm_email_template_slug, $arm_pdfcreator_attachment_templates)){
                        
                        $arm_email_pdf_attachment_status_arr=unserialize($arm_email_pdf_attachment_status);

                        if($arm_social_feature->isSocialFeature){
                            $arm_email_data['arm_email_card_attachment_status']=(isset($arm_email_pdf_attachment_status_arr['arm_email_card_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_email_card_attachment_status']:'';
                        }
                        $arm_invoice_tax_feature = get_option('arm_is_invoice_tax_feature', 0);
                        if($arm_invoice_tax_feature) {    
                            $arm_email_data['arm_email_invoice_attachment_status']=(isset($arm_email_pdf_attachment_status_arr['arm_email_invoice_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_email_invoice_attachment_status']:'';
                        }
                    }
                    
                }
            }    
            return $arm_email_data;
        }
        function arm_pdfcreator_automated_email_attachment_card_invoice_func($arm_message_email_data){
            global $wpdb, $ARMember,$arm_social_feature,$arm_pdfcreator_attachment_templates, $arm_pdfcreator_attachment_templates_for_armcourse;
            $message_id=$arm_message_email_data['id'];
            if(!empty($message_id)){
                $arm_message_email_data['arm_automated_pdfcreator_email_switch']='0';
                $arm_message_email_data['arm_automated_pdfcreator_email_switch_armcourse']='0';
                $arm_message_email_data['arm_automated_email_card_attachment_status']='';
                $arm_message_email_data['arm_automated_email_invoice_attachment_status']='';
                $message_temp_detail = $wpdb->get_row("SELECT arm_message_type,arm_email_pdf_attachment_status FROM `" . $ARMember->tbl_arm_auto_message . "` WHERE `arm_message_id`= '" . $message_id . "' "); //phpcs:ignore
                if(!empty($message_temp_detail)){
                    $arm_message_type=$message_temp_detail->arm_message_type;
                    if(in_array($arm_message_type, $arm_pdfcreator_attachment_templates)){
                        $arm_message_email_data['arm_automated_pdfcreator_email_switch']='1';
                    }
                    $arm_email_pdf_attachment_status=$message_temp_detail->arm_email_pdf_attachment_status;
                    if(!empty($arm_email_pdf_attachment_status) && in_array($arm_message_type,$arm_pdfcreator_attachment_templates)){
                        $arm_email_pdf_attachment_status_arr=unserialize($arm_email_pdf_attachment_status);

                        if($arm_social_feature->isSocialFeature){
                            $arm_message_email_data['arm_automated_email_card_attachment_status']=(isset($arm_email_pdf_attachment_status_arr['arm_automated_email_card_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_automated_email_card_attachment_status']:'';
                        }    
                        $arm_invoice_tax_feature = get_option('arm_is_invoice_tax_feature', 0);
                        if($arm_invoice_tax_feature) {
                            $arm_message_email_data['arm_automated_email_invoice_attachment_status']=(isset($arm_email_pdf_attachment_status_arr['arm_automated_email_invoice_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_automated_email_invoice_attachment_status']:'';
                        }    
                    }
                    if(in_array($arm_message_type, $arm_pdfcreator_attachment_templates_for_armcourse)){
                        $arm_message_email_data['arm_automated_pdfcreator_email_switch_armcourse']='1';
                    }
                    if(!empty($arm_email_pdf_attachment_status) && in_array($arm_message_type,$arm_pdfcreator_attachment_templates_for_armcourse)){
                        $arm_email_pdf_attachment_status_arr=unserialize($arm_email_pdf_attachment_status);
                        $arm_message_email_data['arm_automated_email_certificate_attachment_status']=(isset($arm_email_pdf_attachment_status_arr['arm_automated_email_certificate_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_automated_email_certificate_attachment_status']:'';
                    }
                    
                }
            }    
            return $arm_message_email_data;
        }
        function arm_pdfcreator_email_attachment_save($email_data,$arm_post_data){
            $arm_email_pdf_attachment_status=array();
            if(isset($arm_post_data['arm_email_cardpdf_attachment'])){
                $arm_email_pdf_attachment_status['arm_email_card_attachment_status']=$arm_post_data['arm_email_cardpdf_attachment'];    
            }
            if(isset($arm_post_data['arm_email_invoicepdf_attachment'])){
                $arm_email_pdf_attachment_status['arm_email_invoice_attachment_status']=$arm_post_data['arm_email_invoicepdf_attachment'];
            }
            if(isset($arm_post_data['arm_email_certificatepdf_attachment'])){
                $arm_email_pdf_attachment_status['arm_email_certificate_attachment_status']=$arm_post_data['arm_email_certificatepdf_attachment'];
            }
            $email_data['arm_email_pdf_attachment_status']=serialize($arm_email_pdf_attachment_status);
            return $email_data;
        }
        function arm_pdfcreator_automated_email_attachment_save($email_data,$arm_post_data){
            $arm_email_pdf_attachment_status=array();
            if(isset($arm_post_data['arm_automated_email_cardpdf_attachment'])){
                $arm_email_pdf_attachment_status['arm_automated_email_card_attachment_status']=$arm_post_data['arm_automated_email_cardpdf_attachment'];
            }
            if(isset($arm_post_data['arm_automated_email_invoicepdf_attachment'])){
                $arm_email_pdf_attachment_status['arm_automated_email_invoice_attachment_status']=$arm_post_data['arm_automated_email_invoicepdf_attachment'];
            }
            if(isset($arm_post_data['arm_email_certificatepdf_attachment'])){
                $arm_email_pdf_attachment_status['arm_automated_email_certificate_attachment_status']=$arm_post_data['arm_email_certificatepdf_attachment'];
            }
            $email_data['arm_email_pdf_attachment_status']=serialize($arm_email_pdf_attachment_status);
            return $email_data;
        }
        function arm_pdfcreator_email_attachment_card_invoice_swtich_html($arm_pdf_html){
            global $arm_social_feature,$arm_members_directory;
            $content_html =$arm_pdf_html;
            $arm_invoice_tax_feature = get_option('arm_is_invoice_tax_feature', 0);
            if(!empty($arm_social_feature->isSocialFeature) || !empty($arm_invoice_tax_feature)){
                $content_html .='<tr class="arm_pdfcreator_email_switch" style="display: none;">';
                    if($arm_social_feature->isSocialFeature){
                        $membership_card_template = $arm_members_directory->arm_get_all_membership_card_template();

                        $content_html .='<td>';
                            $content_html .='<div class="arm_email_pdf_content_area_left">';
                                $content_html .='<div class="arm_email_pdf_attachment_lbl">'. __('Attach Membership Card PDF', 'ARMPdfCreator').'</div>';
                                $content_html .='<div class="arm_email_pdf_attachment_section">';
                                    
                                        if(count($membership_card_template)>0){
                                            $content_html .='<div class="armswitch">';
                                                $content_html .='<input type="checkbox" class="armswitch_input arm_email_cardpdf_attachment" id="arm_email_cardpdf_attachment" name="arm_email_cardpdf_attachment">';
                                        
                                                $content_html .='<label class="armswitch_label" for="arm_email_cardpdf_attachment"></label>';
                                             $content_html .='</div>';
                                        }else{
                                            $content_html .='<div class="armswitch arm_left">';
                                                $content_html .='<label class="armswitch_label"></label>';
                                            $content_html .='</div> &nbsp;&nbsp;<span style="color:#ff0000;">'. __('NOTE : Please create card template.', 'ARMPdfCreator').'</span>';
                                        }
                                   
                                $content_html .='</div>';
                               
                            $content_html .='</div>';
                        $content_html .='</td>';
                    
                    }
                    
                    if($arm_invoice_tax_feature) {
                        $content_html .='<td>';
                            $content_html .='<div class="arm_email_pdf_content_area_left">';
                                $content_html .='<div class="arm_email_pdf_attachment_lbl">'. __('Attach invoice PDF', 'ARMPdfCreator').'</div>';
                                $content_html .='<div class="arm_email_pdf_attachment_section">';
                                    $content_html .='<div class="armswitch">';
                                        $content_html .='<input type="checkbox" class="armswitch_input arm_email_invoicepdf_attachment" id="arm_email_invoicepdf_attachment" name="arm_email_invoicepdf_attachment">';
                                        $content_html .='<label class="armswitch_label" for="arm_email_invoicepdf_attachment"></label>';
                                    $content_html .='</div>';
                                $content_html .='</div>';
                            $content_html .='</div>';
                        $content_html .='</td>';
                    }
                $content_html .='</tr>';
            }    
            return $content_html;
        }
        function arm_pdfcreator_automated_email_attachment_card_invoice_swtich_html($arm_pdf_html){
            global $arm_social_feature,$arm_members_directory;
            $content_html =$arm_pdf_html;
            $arm_invoice_tax_feature = get_option('arm_is_invoice_tax_feature', 0);

            if(!empty($arm_social_feature->isSocialFeature) || !empty($arm_invoice_tax_feature)){
                $content_html .='<tr class="arm_pdfcreator_automated_email_switch">';
                    if($arm_social_feature->isSocialFeature){
                        $membership_card_template = $arm_members_directory->arm_get_all_membership_card_template();
                        $content_html .='<td>';
                            $content_html .='<div class="arm_email_pdf_content_area_left">';
                                $content_html .='<div class="arm_email_pdf_attachment_lbl">'.__('Attach Membership Card PDF', 'ARMPdfCreator').'</div>';
                                        $content_html .='<div class="arm_email_pdf_attachment_section">';
                                            
                                                    if(count($membership_card_template)>0){
                                                        $content_html .='<div class="armswitch">';
                                                            $content_html .='<input type="checkbox" class="armswitch_input arm_automated_email_cardpdf_attachment" id="arm_automated_email_cardpdf_attachment" name="arm_automated_email_cardpdf_attachment">';
                                                    
                                                            $content_html .='<label class="armswitch_label" for="arm_automated_email_cardpdf_attachment"></label>';
                                                        $content_html .='</div>';    
                                                    }else{
                                                        $content_html .='<div class="armswitch arm_left">';
                                                            $content_html .='<label class="armswitch_label"></label>';
                                                        $content_html .='</div>';
                                                        $content_html .='&nbsp;&nbsp;<span style="color:#ff0000;">'. __('NOTE : Please create card template.', 'ARMPdfCreator').'</span>';
                                                    }
                                            
                                        $content_html .='</div>';
                                        
                            $content_html .='</div>';
                        $content_html .='</td>';
                    }    
                    
                    if($arm_invoice_tax_feature) {
                        $content_html .='<td>';
                            $content_html .='<div class="arm_email_pdf_content_area_left">';
                                $content_html .='<div class="arm_email_pdf_attachment_lbl">
                                    '.__('Attach invoice PDF', 'ARMPdfCreator').'</div>';
                                $content_html .='<div class="arm_email_pdf_attachment_section">';
                                    $content_html .='<div class="armswitch">';
                                        $content_html .='<input type="checkbox" class="armswitch_input arm_automated_email_invoicepdf_attachment" id="arm_automated_email_invoicepdf_attachment" name="arm_automated_email_invoicepdf_attachment">';
                                        $content_html .='<label class="armswitch_label" for="arm_automated_email_invoicepdf_attachment"></label>';
                                    $content_html .='</div>';
                                $content_html .='</div>';
                            $content_html .='</div>';
                        $content_html .='</td>';
                    }
                $content_html .='</tr>';
            }    
            if( is_plugin_active( 'armembercourses/armembercourses.php' ) ) {
                $content_html .='<tr class="arm_pdfcreator_automated_email_switch_course_certificate">';
                    $content_html .='<td>';
                        $content_html .='<div class="arm_email_pdf_content_area_left">';
                            $content_html .='<div class="arm_email_pdf_attachment_lbl">'. __('Attach certificate PDF', 'ARMPdfCreator').'</div>';
                            $content_html .='<div class="arm_email_pdf_attachment_section">';
                                $content_html .='<div class="armswitch">';
                                    $content_html .='<input type="checkbox" class="armswitch_input arm_email_certificatepdf_attachment" id="arm_email_certificatepdf_attachment" name="arm_email_certificatepdf_attachment">';
                                    $content_html .='<label class="armswitch_label" for="arm_email_certificatepdf_attachment"></label>';
                                $content_html .='</div>';
                            $content_html .='</div>';
                        $content_html .='</div>';
                    $content_html .='</td>';
                $content_html .='</tr>';
            }
            return $content_html;
        }
        function arm_pdfcreator_signup_complete_email_attachments($attachments,$user_id,$email_temp_user,$plan_id){
            global $arm_pdf_constructor,$arm_pdfcreator_attachment_templates,$arm_social_feature, $wp, $wpdb, $ARMember, $arm_pdf_upload_dir_name,$arm_transaction;

            if(!empty($email_temp_user)){
                if(isset($email_temp_user->arm_template_slug) && in_array($email_temp_user->arm_template_slug, $arm_pdfcreator_attachment_templates)){

                    $wp_upload_dir  = wp_upload_dir();
                    $arm_pdf_upload_dir = $wp_upload_dir['basedir'] . '/armember/'.$arm_pdf_upload_dir_name;

                    $arm_invoice_tax_feature = get_option('arm_is_invoice_tax_feature', 0);

                    if(isset($email_temp_user->arm_email_pdf_attachment_status) && !empty($email_temp_user->arm_email_pdf_attachment_status)){
                        $arm_email_pdf_attachment_status_arr=unserialize($email_temp_user->arm_email_pdf_attachment_status);
                        $arm_email_card_attachment_status=(isset($arm_email_pdf_attachment_status_arr['arm_email_card_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_email_card_attachment_status']:'';
                        $arm_email_invoice_attachment_status=(isset($arm_email_pdf_attachment_status_arr['arm_email_invoice_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_email_invoice_attachment_status']:'';

                        $user_plan = 0;  
                        if(!empty($plan_id)){
                            $user_plan = $plan_id; 
                        }
                        $arm_email_pdf_mcard='0';
                        $cards_template = $wpdb->get_results("SELECT * FROM `" . $ARMember->tbl_arm_member_templates . "` WHERE arm_options LIKE '%\"plans\";%' AND arm_type = 'arm_card'", ARRAY_A); //phpcs:ignore
                        if(count($cards_template)>0 && !empty($user_plan)){
                            foreach ($cards_template as $card_index => $card_data) {
                                $card_arm_options=unserialize($card_data['arm_options']);
                                $card_selected_plans=isset($card_arm_options['plans'])?$card_arm_options['plans']:array();
                                if(count($card_selected_plans)>0){
                                    if(in_array($user_plan, $card_selected_plans)){
                                        $arm_email_pdf_mcard=(isset($card_data['arm_id']))?$card_data['arm_id']:'0';
                                    }
                                }    
                            }
                        }
                        
                        if(empty($arm_email_pdf_mcard)){
                            $cards_template = $wpdb->get_results("SELECT * FROM `" . $ARMember->tbl_arm_member_templates . "` WHERE  arm_options NOT LIKE '%\"plans\";%' AND arm_type = 'arm_card' limit 0,1", ARRAY_A);//phpcs:ignore
                            if(count($cards_template)>0){
                                $arm_email_pdf_mcard=(isset($cards_template[0]['arm_id']))?$cards_template[0]['arm_id']:'0';
                            }
                        }
                        if($arm_social_feature->isSocialFeature && $arm_email_card_attachment_status=='on' && !empty($arm_email_pdf_mcard)){
                            $iframe_id='iframe_'.$user_plan.'_'.rand();
                            try {
                                $arm_card_return_data=arm_pdf_card_html_content($arm_email_pdf_mcard,$iframe_id,$user_id,$user_plan,'ARMPDF');
                                if(!empty($arm_card_return_data['arm_card_html_view']) && !empty($arm_card_return_data['card_css'])){
                                    $arm_card_html_view=$arm_card_return_data['arm_card_html_view'];
                                    $card_css=$arm_card_return_data['card_css'];
                                    $user_info = get_userdata($user_id);
                                    $username = $user_info->user_login;
                                    $first_name = $user_info->first_name;
                                    $last_name = $user_info->last_name;
                                    $pdf_template_name='card_'.$user_id.$username;
                                    $arm_pdf_title='User Card';
                                    if(!empty($first_name) || $last_name){
                                        $arm_pdf_title=$first_name.''.$last_name;
                                    }else{
                                        $arm_pdf_title=$username;
                                    }
                                    $arm_pdfcreator_mpdf3 = new Mpdf\Mpdf($arm_pdf_constructor);

                                    $arm_pdfcreator_mpdf3->setAutoTopMargin = 'stretch';
                                    $arm_pdfcreator_mpdf3->setAutoBottomMargin = 'stretch';
                                    $arm_pdfcreator_mpdf3->autoScriptToLang = true;
                                    $arm_pdfcreator_mpdf3->baseScript = 1;
                                    $arm_pdfcreator_mpdf3->autoVietnamese = true;
                                    $arm_pdfcreator_mpdf3->autoArabic = true;
                                    $arm_pdfcreator_mpdf3->autoLangToFont = true;
                                    if(is_rtl())
                                    {
                                        $arm_pdfcreator_mpdf3->SetDirectionality('rtl');
                                    }
                                    $card_temps_data = $wpdb->get_results("SELECT arm_options FROM `" . $ARMember->tbl_arm_member_templates . "` WHERE arm_id = {$arm_email_pdf_mcard} AND arm_type = 'arm_card' ", ARRAY_A); //phpcs:ignore
                            
                                    if(!empty($card_temps_data)){
                                        $card_temps_data = array_column($card_temps_data, "arm_options");
                                        $card_data_opts = maybe_unserialize($card_temps_data[0]);    
                                        
                                        $arm_card_ttl_font_family = !empty($card_data_opts["title_font"]["font_family"]) ? $card_data_opts["title_font"]["font_family"] : "";
                                        if(!empty($arm_card_ttl_font_family)){
                                            $arm_card_ttl_font_family_replace=str_replace(' ', '-', strtolower($arm_card_ttl_font_family));
                                            $card_css=str_replace($arm_card_ttl_font_family,$arm_card_ttl_font_family_replace, $card_css);
                                        }

                                         $arm_card_lbl_font_family = !empty($card_data_opts["label_font"]["font_family"]) ? $card_data_opts["label_font"]["font_family"] : "";
                                        if(!empty($arm_card_lbl_font_family)){
                                            $arm_card_lbl_font_family_replace=str_replace(' ', '-', strtolower($arm_card_lbl_font_family));
                                            $card_css=str_replace($arm_card_lbl_font_family,$arm_card_lbl_font_family_replace, $card_css);
                                        }
                                         $arm_card_content_font_family = !empty($card_data_opts["content_font"]["font_family"]) ? $card_data_opts["content_font"]["font_family"] : "";
                                         if(!empty($arm_card_content_font_family)){
                                            $arm_card_content_font_family_replace=str_replace(' ', '-', strtolower($arm_card_content_font_family));
                                            $card_css=str_replace($arm_card_content_font_family,$arm_card_content_font_family_replace, $card_css);
                                        }
                                    }
                                    $arm_pdfcreator_mpdf3->WriteHTML($card_css,1);
                                    $arm_pdfcreator_mpdf3->WriteHTML($arm_card_html_view,2);
                                    $arm_pdfcreator_mpdf3->SetTitle($arm_pdf_title);
                                    $card_pdffilename    = $arm_pdf_upload_dir.'/'.$pdf_template_name.'.pdf';
                                    $arm_pdfcreator_mpdf3->Output($card_pdffilename, 'F');
                                    $attachments[]=$card_pdffilename;
                                    
                                }
                            } catch (\Mpdf\MpdfException $e) {
                                    
                                $ARMember->arm_write_response('Exception in PDF create for Card template in standard Email notification => '.addslashes($e->getMessage()).' in '.$e->getFile().' on line '.$e->getLine());
                            }    
                        }  
                            
                        if($arm_invoice_tax_feature=='1' && $arm_email_invoice_attachment_status=='on' && !empty($user_plan)){

                            $armLogTable = $ARMember->tbl_arm_payment_log;
                            $selectColumns = '`arm_log_id`, arm_invoice_id';
                            
                            $log_detail = $wpdb->get_row("SELECT {$selectColumns} FROM `{$armLogTable}` WHERE `arm_user_id`='{$user_id}' AND `arm_plan_id`='{$user_plan}' ORDER BY `arm_log_id` DESC"); //phpcs:ignore
                            if (!empty($log_detail)) {
                                $user_arm_log_id = $log_detail->arm_log_id;

                                $invoice_css='body {margin: 0;padding: 0;font: 12pt "Tahoma";}.page {width: 700px;min-height: 600px;padding: 20px;margin: 0 auto;background: white;}';
                                $arm_invoice_html_view='';
                                if (!empty($user_arm_log_id) && $user_arm_log_id != 0) {
                                    try {
                                        
                                        $arm_invoice_html_view .=arm_pdf_invoice_html_content($user_arm_log_id);      
                                        
                                        $arm_pdf_title=__('Invoice','ARMPdfCreator').' #'.$log_detail->arm_invoice_id;
                                        $pdf_template_name='invoice_'.$user_id.$log_detail->arm_invoice_id;
                                        
                                        $arm_invoice_html_doc = new DOMDocument();
                                        $arm_invoice_html_doc->loadHTML($arm_invoice_html_view);
                                        $style_output = $arm_invoice_html_doc->getElementsByTagName("style");
                                        if($style_output->length>0){
                                            for ($i=0; $i < $style_output->length; $i++) { 
                                                $invoice_css .=$style_output->item($i)->nodeValue;;
                                            }
                                        }
                                        $arm_invoice_html_view = preg_replace('/<style\b[^>]*>(.*?)<\/style>/i', '', $arm_invoice_html_view);
                                        $arm_invoice_html_view = preg_replace('/<script\b[^>]*>(.*?)<\/script>/i', '', $arm_invoice_html_view);
                                        $arm_pdfcreator_mpdf1 = new Mpdf\Mpdf($arm_pdf_constructor);

                                        $arm_pdfcreator_mpdf1->setAutoTopMargin = 'stretch';
                                        $arm_pdfcreator_mpdf1->setAutoBottomMargin = 'stretch';
                                        $arm_pdfcreator_mpdf1->autoScriptToLang = true;
                                        $arm_pdfcreator_mpdf1->baseScript = 1;
                                        $arm_pdfcreator_mpdf1->autoVietnamese = true;
                                        $arm_pdfcreator_mpdf1->autoArabic = true;
                                        $arm_pdfcreator_mpdf1->autoLangToFont = true;
                                        if(is_rtl())
                                        {
                                            $arm_pdfcreator_mpdf1->SetDirectionality('rtl');
                                        }
                                        $arm_pdfcreator_mpdf1->WriteHTML($invoice_css,1);
                                        $arm_pdfcreator_mpdf1->WriteHTML($arm_invoice_html_view,2);
                                        $arm_pdfcreator_mpdf1->SetTitle($arm_pdf_title);
                                        $pdffilename    = $arm_pdf_upload_dir.'/'.$pdf_template_name.'.pdf';
                                        $arm_pdfcreator_mpdf1->Output($pdffilename, 'F');
                                        $attachments[]=$pdffilename;

                                    } catch (\Mpdf\MpdfException $e) {
                                            
                                        $ARMember->arm_write_response('Exception in PDF create for Invoice standard Email notification=> '.addslashes($e->getMessage()).' in '.$e->getFile().' on line '.$e->getLine());
                                    }    
                                }   
                            }    
                        }
                    }    

                    
                }
            }
            return $attachments;
        }
        function arm_pdfcreator_auto_message_email_attachments($attachments,$user_id,$email_temp_user,$plan_id){
            global $arm_pdf_constructor,$arm_pdfcreator_attachment_templates,$arm_social_feature, $wp, $wpdb, $ARMember, $arm_pdf_upload_dir_name,$arm_transaction;
            
            if(!empty($email_temp_user)){
                if(isset($email_temp_user->arm_message_type) && in_array($email_temp_user->arm_message_type, $arm_pdfcreator_attachment_templates)){
                    
                    $wp_upload_dir  = wp_upload_dir();
                    $arm_pdf_upload_dir = $wp_upload_dir['basedir'] . '/armember/'.$arm_pdf_upload_dir_name;

                    $arm_invoice_tax_feature = get_option('arm_is_invoice_tax_feature', 0);

                    if(isset($email_temp_user->arm_email_pdf_attachment_status) && !empty($email_temp_user->arm_email_pdf_attachment_status)){
                        
                        $arm_email_pdf_attachment_status_arr=unserialize($email_temp_user->arm_email_pdf_attachment_status);
                        $arm_email_card_attachment_status=(isset($arm_email_pdf_attachment_status_arr['arm_automated_email_card_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_automated_email_card_attachment_status']:'';
                        
                        $arm_email_invoice_attachment_status=(isset($arm_email_pdf_attachment_status_arr['arm_automated_email_invoice_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_automated_email_invoice_attachment_status']:'';

                        $user_plan = 0;  
                        if(!empty($plan_id)){
                            $user_plan = $plan_id; 
                        }
                        $arm_automated_email_pdf_mcard='0';
                        $cards_template = $wpdb->get_results("SELECT * FROM `" . $ARMember->tbl_arm_member_templates . "` WHERE arm_options LIKE '%\"plans\";%' AND arm_type = 'arm_card'", ARRAY_A); //phpcs:ignore
                        if(count($cards_template)>0 && !empty($user_plan)){
                            foreach ($cards_template as $card_index => $card_data) {
                                $card_arm_options=unserialize($card_data['arm_options']);
                                $card_selected_plans=isset($card_arm_options['plans'])?$card_arm_options['plans']:array();
                                if(count($card_selected_plans)>0){
                                    if(in_array($user_plan, $card_selected_plans)){
                                        $arm_automated_email_pdf_mcard=(isset($card_data['arm_id']))?$card_data['arm_id']:'0';
                                    }
                                }    
                            }
                        }
                        
                        if(empty($arm_automated_email_pdf_mcard)){
                            $cards_template = $wpdb->get_results("SELECT * FROM `" . $ARMember->tbl_arm_member_templates . "` WHERE  arm_options NOT LIKE '%\"plans\";%' AND arm_type = 'arm_card' limit 0,1", ARRAY_A); //phpcs:ignore
                            if(count($cards_template)>0){
                                $arm_automated_email_pdf_mcard=(isset($cards_template[0]['arm_id']))?$cards_template[0]['arm_id']:'0';
                            }
                        }
                        if($arm_social_feature->isSocialFeature && $arm_email_card_attachment_status=='on' && !empty($arm_automated_email_pdf_mcard)){
                            $iframe_id='iframe_'.$user_plan.'_'.rand();
                            try {
                                $arm_card_return_data=arm_pdf_card_html_content($arm_automated_email_pdf_mcard,$iframe_id,$user_id,$user_plan,'ARMPDF');
                                if(!empty($arm_card_return_data['arm_card_html_view']) && !empty($arm_card_return_data['card_css'])){
                                    $arm_card_html_view=$arm_card_return_data['arm_card_html_view'];
                                    $card_css=$arm_card_return_data['card_css'];
                                    $user_info = get_userdata($user_id);
                                    $username = $user_info->user_login;
                                    $first_name = $user_info->first_name;
                                    $last_name = $user_info->last_name;
                                    $pdf_template_name='card_'.$user_id.$username;
                                    $arm_pdf_title='User Card';
                                    if(!empty($first_name) || $last_name){
                                        $arm_pdf_title=$first_name.''.$last_name;
                                    }else{
                                        $arm_pdf_title=$username;
                                    }
                                    $arm_pdfcreator_mpdf = new Mpdf\Mpdf($arm_pdf_constructor);

                                    $arm_pdfcreator_mpdf->setAutoTopMargin = 'stretch';
                                    $arm_pdfcreator_mpdf->setAutoBottomMargin = 'stretch';
                                    $arm_pdfcreator_mpdf->autoScriptToLang = true;
                                    $arm_pdfcreator_mpdf->baseScript = 1;
                                    $arm_pdfcreator_mpdf->autoVietnamese = true;
                                    $arm_pdfcreator_mpdf->autoArabic = true;
                                    $arm_pdfcreator_mpdf->autoLangToFont = true;
                                    if(is_rtl())
                                    {
                                        $arm_pdfcreator_mpdf->SetDirectionality('rtl');
                                    }
                                     $card_temps_data = $wpdb->get_results("SELECT arm_options FROM `" . $ARMember->tbl_arm_member_templates . "` WHERE arm_id = {$arm_automated_email_pdf_mcard} AND arm_type = 'arm_card' ", ARRAY_A); //phpcs:ignore
                            
                                    if(!empty($card_temps_data)){
                                        $card_temps_data = array_column($card_temps_data, "arm_options");
                                        $card_data_opts = maybe_unserialize($card_temps_data[0]);    
                                        
                                        $arm_card_ttl_font_family = !empty($card_data_opts["title_font"]["font_family"]) ? $card_data_opts["title_font"]["font_family"] : "";
                                        if(!empty($arm_card_ttl_font_family)){
                                            $arm_card_ttl_font_family_replace=str_replace(' ', '-', strtolower($arm_card_ttl_font_family));
                                            $card_css=str_replace($arm_card_ttl_font_family,$arm_card_ttl_font_family_replace, $card_css);
                                        }

                                         $arm_card_lbl_font_family = !empty($card_data_opts["label_font"]["font_family"]) ? $card_data_opts["label_font"]["font_family"] : "";
                                        if(!empty($arm_card_lbl_font_family)){
                                            $arm_card_lbl_font_family_replace=str_replace(' ', '-', strtolower($arm_card_lbl_font_family));
                                            $card_css=str_replace($arm_card_lbl_font_family,$arm_card_lbl_font_family_replace, $card_css);
                                        }
                                         $arm_card_content_font_family = !empty($card_data_opts["content_font"]["font_family"]) ? $card_data_opts["content_font"]["font_family"] : "";
                                         if(!empty($arm_card_content_font_family)){
                                            $arm_card_content_font_family_replace=str_replace(' ', '-', strtolower($arm_card_content_font_family));
                                            $card_css=str_replace($arm_card_content_font_family,$arm_card_content_font_family_replace, $card_css);
                                        }
                                    }
                                    $arm_pdfcreator_mpdf->WriteHTML($card_css,1);
                                    $arm_pdfcreator_mpdf->WriteHTML($arm_card_html_view,2);
                                    $arm_pdfcreator_mpdf->SetTitle($arm_pdf_title);
                                    $card_pdffilename    = $arm_pdf_upload_dir.'/'.$pdf_template_name.'.pdf';
                                    $arm_pdfcreator_mpdf->Output($card_pdffilename, 'F');
                                    $attachments[]=$card_pdffilename;
                                    
                                }
                            } catch (\Mpdf\MpdfException $e) {
                                    
                                $ARMember->arm_write_response('Exception in PDF create for Card auto Email notification=> '.addslashes($e->getMessage()).' in '.$e->getFile().' on line '.$e->getLine());
                            }
                        }  
                            
                        if($arm_invoice_tax_feature=='1' && $arm_email_invoice_attachment_status=='on' && !empty($user_plan)){
                            
                            $armLogTable = $ARMember->tbl_arm_payment_log;
                            $selectColumns = '`arm_log_id`, arm_invoice_id';
                            
                            $log_detail = $wpdb->get_row("SELECT {$selectColumns} FROM `{$armLogTable}` WHERE `arm_user_id`='{$user_id}' AND `arm_plan_id`='{$user_plan}' ORDER BY `arm_log_id` DESC"); //phpcs:ignore
                            if (!empty($log_detail)) {
                                $user_arm_log_id = $log_detail->arm_log_id;
                                
                                $invoice_css='body {margin: 0;padding: 0;font: 12pt "Tahoma";}.page {width: 700px;min-height: 600px;padding: 20px;margin: 0 auto;background: white;}';
                                $arm_invoice_html_view='';
                                if (!empty($user_arm_log_id) && $user_arm_log_id != 0) {
                                    try {
                                        
                                        $arm_invoice_html_view .=arm_pdf_invoice_html_content($user_arm_log_id);      
                                        
                                        $arm_pdf_title=__('Invoice','ARMPdfCreator').' #'.$log_detail->arm_invoice_id;
                                        $pdf_template_name='invoice_'.$user_id.$log_detail->arm_invoice_id;
                                        
                                        $arm_invoice_html_doc = new DOMDocument();
                                        $arm_invoice_html_doc->loadHTML($arm_invoice_html_view);
                                        $style_output = $arm_invoice_html_doc->getElementsByTagName("style");
                                        if($style_output->length>0){
                                            for ($i=0; $i < $style_output->length; $i++) { 
                                                $invoice_css .=$style_output->item($i)->nodeValue;;
                                            }
                                        }
                                        $arm_invoice_html_view = preg_replace('/<style\b[^>]*>(.*?)<\/style>/i', '', $arm_invoice_html_view);
                                        $arm_invoice_html_view = preg_replace('/<script\b[^>]*>(.*?)<\/script>/i', '', $arm_invoice_html_view);
                                        
                                        $arm_pdfcreator_mpdf2 = new Mpdf\Mpdf($arm_pdf_constructor);

                                        $arm_pdfcreator_mpdf2->setAutoTopMargin = 'stretch';
                                        $arm_pdfcreator_mpdf2->setAutoBottomMargin = 'stretch';
                                        $arm_pdfcreator_mpdf2->autoScriptToLang = true;
                                        $arm_pdfcreator_mpdf2->baseScript = 1;
                                        $arm_pdfcreator_mpdf2->autoVietnamese = true;
                                        $arm_pdfcreator_mpdf2->autoArabic = true;
                                        $arm_pdfcreator_mpdf2->autoLangToFont = true;
                                        if(is_rtl())
                                        {
                                            $arm_pdfcreator_mpdf2->SetDirectionality('rtl');
                                        }
                                        $arm_pdfcreator_mpdf2->WriteHTML($invoice_css,1);
                                        $arm_pdfcreator_mpdf2->WriteHTML($arm_invoice_html_view,2);
                                        $arm_pdfcreator_mpdf2->SetTitle($arm_pdf_title);
                                        $pdffilename    = $arm_pdf_upload_dir.'/'.$pdf_template_name.'.pdf';
                                        $arm_pdfcreator_mpdf2->Output($pdffilename, 'F');
                                        $attachments[]=$pdffilename;
                                        
                                    } catch (\Mpdf\MpdfException $e) {
                                            
                                        $ARMember->arm_write_response('Exception in PDF create for Invoice auto Email notification=> '.addslashes($e->getMessage()).' in '.$e->getFile().' on line '.$e->getLine());
                                    }    
                                }   
                            }    
                        }
                    }    

                    
                }
                if(isset($email_temp_user->arm_message_type) && $email_temp_user->arm_message_type=='arm_courses_notify_when_complete_course'){
                    $arm_email_pdf_attachment_status_arr=unserialize($email_temp_user->arm_email_pdf_attachment_status);
                    $arm_email_certificate_attachment_status=(isset($arm_email_pdf_attachment_status_arr['arm_automated_email_certificate_attachment_status']))?$arm_email_pdf_attachment_status_arr['arm_automated_email_certificate_attachment_status']:'';
                    if($arm_email_certificate_attachment_status=='on'){
                        global $arm_courses_settings, $armcourse_lessons_tbl_name;
                        
                        $arm_course_lesson_id = !empty($_REQUEST['arm_course_lesson_id'])?intval($_REQUEST['arm_course_lesson_id']):0;
    
                        $lessson_query = "SELECT course_id FROM ".$armcourse_lessons_tbl_name." WHERE lesson_status=1 AND id = '".$arm_course_lesson_id."'";
                        $get_lesson_details = $wpdb->get_row($lessson_query); //phpcs:ignore
                        $course_id = !empty($get_lesson_details->course_id)?$get_lesson_details->course_id:0;
    
                        $wp_upload_dir  = wp_upload_dir();
                        $arm_pdf_upload_dir = $wp_upload_dir['basedir'] . '/armember/'.$arm_pdf_upload_dir_name;
                        $current_user = wp_get_current_user();
                        $user_id = $current_user->ID;
                        $user_info = get_userdata($user_id);
                        $username = $user_info->user_login;
                        $certificate_css = '@page {  margin: 42px; }@page .arm_course_certificate{padding:40px;}.arm-course-certificate-02-box .arm-body-sec .arm-body-name::after{display:none;}.arm-course-certificate-03-box .arm-body-sec .arm-body-name::after{display:none;}';
    
                        $arm_courses_option_settings = $arm_courses_settings->get_arm_courses_settings();
                        $arm_courses_active_certificate = $arm_courses_option_settings['arm_courses_active_certificate'];
                        $page_size = ($arm_courses_active_certificate =='certificate3')? 'P':'L';
    
                        $view_type="ARMPDF";
                        $arm_certificate_get_content = $arm_courses_settings->arm_course_get_certificate_html($arm_courses_active_certificate, $course_id, $view_type);
                        $arm_certificate_html_view = $arm_certificate_get_content['arm_courses_certificate_content'];
                        $certificate_css .= $arm_certificate_get_content['arm_courses_certificate_css'];
                        
                        $arm_pdf_title = __('Certificate','ARMPdfCreator');
                        $pdf_template_name = 'certificate_' . intval($_REQUEST['arm_course_lesson_id']) . '_'.$username;
    
                        $arm_certificate_html_view = preg_replace('/<style\b[^>]*>(.*?)<\/style>/i', '', $arm_certificate_html_view);
                        $arm_certificate_html_view = preg_replace('/<script\b[^>]*>(.*?)<\/script>/i', '', $arm_certificate_html_view);
                        $arm_pdf_format = 'A4';
                        if($arm_courses_active_certificate =='certificate1'){
                            $arm_pdf_format = [277, 375];
                        }
                        $arm_pdf_constructor['format'] = $arm_pdf_format;
                        $arm_pdf_constructor['orientation'] = $page_size;
    
                        $arm_pdfcreator_mpdf = new Mpdf\Mpdf($arm_pdf_constructor);
    
                        $arm_pdfcreator_mpdf->setAutoTopMargin = 'stretch';
                        $arm_pdfcreator_mpdf->setAutoBottomMargin = 'stretch';
                        $arm_pdfcreator_mpdf->autoScriptToLang = true;
                        $arm_pdfcreator_mpdf->baseScript = 1;
                        $arm_pdfcreator_mpdf->autoVietnamese = true;
                        $arm_pdfcreator_mpdf->autoArabic = true;
                        $arm_pdfcreator_mpdf->autoLangToFont = true;
    
                        if(is_rtl())
                        {
                            $arm_pdfcreator_mpdf->SetDirectionality('rtl');
                        }
    
                        $arm_pdfcreator_mpdf->WriteHTML($certificate_css,1);
                        $arm_pdfcreator_mpdf->WriteHTML($arm_certificate_html_view,2);
                        $arm_pdfcreator_mpdf->SetTitle($arm_pdf_title);
                        $card_pdffilename    = $arm_pdf_upload_dir.'/'.$pdf_template_name.'.pdf';
                        $arm_pdfcreator_mpdf->Output($card_pdffilename, 'F');
                        $attachments[]=$card_pdffilename;
                    }
                }
            }
           
            return $attachments;
        }
    }

}
global $arm_pdfcreator_general_settings;
$arm_pdfcreator_general_settings = new ARM_PdfCreator_General_Settings();
?>